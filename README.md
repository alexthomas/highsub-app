# Highsub app aka aniclips

## Description
This is the user interface for the Highsub app. It is a web app that allows user to search for anime clips 
by the subtitles and share them with others. 

## Installation
1. Clone the repository
2. Configure the build environment variables
   - FONTAWESOME_NPM_AUTH_TOKEN: required to install fontawesome pro
3. Install dependencies
   - `npm install`
4. Configure the application environment variables
   - `cp .env.example .env`
   - update the appwrite api key
5. Run the application
   - `npm run dev`

## [UX Bible](documentation/ux)


