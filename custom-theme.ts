
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const highsubTheme: CustomThemeConfig = {
  name: 'highsub',
  properties: {
    // =~= Theme Properties =~=
    "--theme-font-family-base": `roboto, system-ui`,
    "--theme-font-family-heading": `roboto, system-ui`,
    "--theme-font-color-base": "0 0 0",
    "--theme-font-color-dark": "255 255 255",
    "--theme-rounded-base": "8px",
    "--theme-rounded-container": "8px",
    "--theme-border-base": "1px",
    // =~= Theme On-X Colors =~=
    "--on-primary": "0 0 0",
    "--on-secondary": "0 0 0",
    "--on-tertiary": "0 0 0",
    "--on-success": "0 0 0",
    "--on-warning": "0 0 0",
    "--on-error": "255 255 255",
    "--on-surface": "255 255 255",
    // =~= Theme Colors  =~=
    // primary | #0a9ddb
    "--color-primary-50": "218 240 250", // #daf0fa
    "--color-primary-100": "206 235 248", // #ceebf8
    "--color-primary-200": "194 231 246", // #c2e7f6
    "--color-primary-300": "157 216 241", // #9dd8f1
    "--color-primary-400": "84 186 230", // #54bae6
    "--color-primary-500": "10 157 219", // #0a9ddb
    "--color-primary-600": "9 141 197", // #098dc5
    "--color-primary-700": "8 118 164", // #0876a4
    "--color-primary-800": "6 94 131", // #065e83
    "--color-primary-900": "5 77 107", // #054d6b
    // secondary | #54b72a
    "--color-secondary-50": "229 244 223", // #e5f4df
    "--color-secondary-100": "221 241 212", // #ddf1d4
    "--color-secondary-200": "212 237 202", // #d4edca
    "--color-secondary-300": "187 226 170", // #bbe2aa
    "--color-secondary-400": "135 205 106", // #87cd6a
    "--color-secondary-500": "84 183 42", // #54b72a
    "--color-secondary-600": "76 165 38", // #4ca526
    "--color-secondary-700": "63 137 32", // #3f8920
    "--color-secondary-800": "50 110 25", // #326e19
    "--color-secondary-900": "41 90 21", // #295a15
    // tertiary | #e70dd5
    "--color-tertiary-50": "251 219 249", // #fbdbf9
    "--color-tertiary-100": "250 207 247", // #facff7
    "--color-tertiary-200": "249 195 245", // #f9c3f5
    "--color-tertiary-300": "245 158 238", // #f59eee
    "--color-tertiary-400": "238 86 226", // #ee56e2
    "--color-tertiary-500": "231 13 213", // #e70dd5
    "--color-tertiary-600": "208 12 192", // #d00cc0
    "--color-tertiary-700": "173 10 160", // #ad0aa0
    "--color-tertiary-800": "139 8 128", // #8b0880
    "--color-tertiary-900": "113 6 104", // #710668
    // success | #3ccd74
    "--color-success-50": "226 248 234", // #e2f8ea
    "--color-success-100": "216 245 227", // #d8f5e3
    "--color-success-200": "206 243 220", // #cef3dc
    "--color-success-300": "177 235 199", // #b1ebc7
    "--color-success-400": "119 220 158", // #77dc9e
    "--color-success-500": "60 205 116", // #3ccd74
    "--color-success-600": "54 185 104", // #36b968
    "--color-success-700": "45 154 87", // #2d9a57
    "--color-success-800": "36 123 70", // #247b46
    "--color-success-900": "29 100 57", // #1d6439
    // warning | #dcab18
    "--color-warning-50": "250 242 220", // #faf2dc
    "--color-warning-100": "248 238 209", // #f8eed1
    "--color-warning-200": "246 234 197", // #f6eac5
    "--color-warning-300": "241 221 163", // #f1dda3
    "--color-warning-400": "231 196 93", // #e7c45d
    "--color-warning-500": "220 171 24", // #dcab18
    "--color-warning-600": "198 154 22", // #c69a16
    "--color-warning-700": "165 128 18", // #a58012
    "--color-warning-800": "132 103 14", // #84670e
    "--color-warning-900": "108 84 12", // #6c540c
    // error | #d1311f
    "--color-error-50": "248 224 221", // #f8e0dd
    "--color-error-100": "246 214 210", // #f6d6d2
    "--color-error-200": "244 204 199", // #f4ccc7
    "--color-error-300": "237 173 165", // #edada5
    "--color-error-400": "223 111 98", // #df6f62
    "--color-error-500": "209 49 31", // #d1311f
    "--color-error-600": "188 44 28", // #bc2c1c
    "--color-error-700": "157 37 23", // #9d2517
    "--color-error-800": "125 29 19", // #7d1d13
    "--color-error-900": "102 24 15", // #66180f
    // surface | #313235
    "--color-surface-50": "224 224 225", // #e0e0e1
    "--color-surface-100": "214 214 215", // #d6d6d7
    "--color-surface-200": "204 204 205", // #cccccd
    "--color-surface-300": "173 173 174", // #adadae
    "--color-surface-400": "111 112 114", // #6f7072
    "--color-surface-500": "49 50 53", // #313235
    "--color-surface-600": "44 45 48", // #2c2d30
    "--color-surface-700": "37 38 40", // #252628
    "--color-surface-800": "29 30 32", // #1d1e20
    "--color-surface-900": "24 25 26", // #18191a

  }
}