import express = require("express");
import * as admin from "firebase-admin";
import {COLLECTIONS} from "./types";

const {ARTICLES} = COLLECTIONS;

export const routes = express();
routes.get("/", async (req, res) => {
  const articles = await getArticles();
  res.json(articles);
});


/**
 * Get articles
 * @return {Promise<unknown>}
 */
async function getArticles(): Promise<unknown> {
  const snapshot = await admin.firestore().collection(ARTICLES).get();
  return snapshot.docs.map((doc) => doc.data());
}
