import * as admin from "firebase-admin";
import express = require("express");

export const routes = express();

/**
 * Check if the user is an admin
 * @param {string|null} idToken The user's token
 * @return {Promise<boolean>} Whether the user is an admin. Defaults to false if no token is provided.
 */
export async function isAdmin(idToken: string | null): Promise<boolean> {
  if (!idToken) return false;
  const decodedToken = await admin.auth().verifyIdToken(idToken);
  return decodedToken?.admin || false;
}

routes.post("/check-admin", async (req, res) => {
  const {idToken} = req.body;
  const admin = await isAdmin(idToken);
  res.json({admin});
});
