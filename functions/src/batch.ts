/* eslint-disable */
import {firestore} from "firebase-admin";
import {getSortKey, mapEpisode} from "./cleansingUtilities";
import {Episode, Show, SlimEpisode, SlimShow} from "./types";
import * as functions from "firebase-functions";


/**
 * Migrate episodes from the old structure to the new structure
 */
async function migrateEpisodes() {
  try {
    // Get a reference to the source collection
    const sourceCollection = firestore().collectionGroup("episodes");

    // Read documents from the source collection
    const snapshot = await sourceCollection.get();
    let writeBatch = firestore().batch();

    // Iterate through each document
    for (let i = 0; i < snapshot.docs.length; i++) {
      const doc = snapshot.docs[i];
      const data = mapEpisode(doc.data() as Episode);

      // Get the parent document ID
      const showId = doc.ref.parent.parent?.id;

      // Construct the destination document path
      const destinationPath = `/shows/${showId}/publicEpisodes/${doc.id}`;

      // Write the data to the destination collection
      writeBatch.set(firestore().doc(destinationPath), data);

      // Commit the batch every 500 documents
      if (i % 500 === 0) {
        await writeBatch.commit();
        writeBatch = firestore().batch();
        console.log(`Processed ${i} documents`);
      }
    }
    await writeBatch.commit();
  } catch (error) {
    console.error("Error migrating documents:", error);
    throw error;
  }
}


// eslint-disable-next-line require-jsdoc, @typescript-eslint/no-unused-vars
async function createPublicShowsCatalog() {
  const showsSnapshot = await firestore().collection("shows").get();
  const shows:SlimShow[] = showsSnapshot.docs
      .map((doc) => doc.data() as Show)
      .filter((s: Show) => s.indexedEpisodeCount > 0)
  // eslint-disable-next-line max-len
      .map(({show, indexedEpisodeCount, tags, summary}) => ({show, indexedEpisodeCount, tags, summary}));
  const catalogItem = {
    shows,
    public: true,
  };
  await firestore().collection("catalogs").doc("shows").set(catalogItem);
}

// eslint-disable-next-line require-jsdoc, @typescript-eslint/no-unused-vars
async function createPrivateShowsCatalog() {
  const showsSnapshot = await firestore().collection("shows").get();
  const shows: SlimShow[] = showsSnapshot.docs
      .map((doc) => doc.data() as Show)
  // eslint-disable-next-line max-len
      .map(({show, indexedEpisodeCount, tags, summary}) => ({show, indexedEpisodeCount, tags, summary}));
  const catalogItem = {
    shows,
    public: false,
  };
  await firestore().collection("catalogs").doc("adminShows").set(catalogItem);
}


// eslint-disable-next-line require-jsdoc
async function createPublicEpisodesCatalog() {
  const episodesSnapshot = await firestore().collectionGroup("episodes").get();
  const episodesByShow: { [key: string]: SlimEpisode[] } = {};
  episodesSnapshot.forEach((d) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const showKey = d.ref.parent.parent!.id;
    if (!episodesByShow[showKey]) {
      episodesByShow[showKey] = [];
    }
    const {show, season, episode, subtitleCount, title} = d.data() as Episode;
    // eslint-disable-next-line max-len
    const slimEpisode:SlimEpisode = {show, season, episode, subtitleCount, title, id: d.id, sortKey: getSortKey(d.data() as Episode)};

    episodesByShow[showKey].push(slimEpisode);
  });
  for (const key of Object.keys(episodesByShow)) {
    const catalogItem = {
      episodes: episodesByShow[key],
      public: true,
    };
    await firestore().collection("catalogs")
        .doc(key + "-episodes").set(catalogItem);
  }
}

// eslint-disable-next-line require-jsdoc
export const createCatalogs = functions.https.onRequest(async (req, res) => {
  await Promise.all([
    createPublicShowsCatalog(),
    createPrivateShowsCatalog(),
    createPublicEpisodesCatalog(),
  ]);
  res.send("Catalogs created");
});

// eslint-disable-next-line require-jsdoc
export const createShowsCatalogs = functions.https.onRequest(async (req, res) => {
  await Promise.all([
    createPublicShowsCatalog()
  ]);
  res.send("Catalogs created");
});

// eslint-disable-next-line max-len
export const migrateEpisodesFunction = functions.https.onRequest(async (req, res) => {
  await migrateEpisodes();
  res.send("Episodes migrated");
});


