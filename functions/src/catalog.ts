import express = require("express");
import createError = require( "http-errors");
import * as admin from "firebase-admin";
import {COLLECTIONS} from "./types";
import {isAdmin} from "./auth";

const {CATALOGS} = COLLECTIONS;
export const routes = express();

routes.get("/:key", async (req, res) => {
  const key = req.params.key;
  const idToken = req.headers.authorization?.split("Bearer ")[1] ?? null;
  const catalog = await getCatalog(key, idToken);
  res.json(catalog);
});

/**
 * Gets a catalog
 * @param {string} key The key of the catalog
 * @param {string|null} idToken The id token of the user
 * @return {Promise<unknown>} The catalog
 */
async function getCatalog(key: string, idToken: string|null): Promise<unknown> {
  if (!key.startsWith(CATALOGS)) {
    throw createError(404, "Not Found");
  }
  const snapshot = await admin.firestore().doc(key).get();
  const data = snapshot.data();
  if (!data?.public && !(await isAdmin(idToken))) {
    throw createError(403, "Forbidden");
  }
  return data;
}
