// eslint-disable-next-line max-len
import type {Episode, PublicEpisode} from "./types";

/**
 * Maps an episode object to a client episode object
 * @param {Episode} episodeObject The episode object to map
 * @return {PublicEpisode} ClientEpisode The mapped client episode object
 */
export function mapEpisode(episodeObject: Episode): PublicEpisode {
  const {
    show, season, episode, endingStart, endingEnd
    , openingEnd, openingStart, originallyAvailableAt, studio
    , subtitleCount, summary, title, height, width,
  } = episodeObject;
  const sortKey = getSortKey(episodeObject);
  return {
    show, season, episode, endingStart, endingEnd, openingEnd,
    openingStart, originallyAvailableAt,
    studio, subtitleCount, summary, title, height, width, sortKey,
  };
}

/**
 * Gets the sort key for an episode. The sort key is used to sort episodes in
 * a show. Numbers should be padded and the season number should be extracted
 * from the season
 * @param {Episode} episode The episode to get the sort key for
 * @return {string} The sort key for the episode
 */
export function getSortKey(episode: Episode): string {
  const show = episode.show;
  const season = getSeasonNumber(episode.season);
  const episodeNumber = episode.episode.padStart(5, "0");
  return `${show}-${season}-${episodeNumber}`;
}

/**
 * Gets the season number from a season string.
 * If the season is not formatted as `Season <number>`,
 * the season number is returned as is;
 * @param {string} season The season string to get the season number from
 * @return {string} The season number
 */
function getSeasonNumber(season: string): string {
  const seasonMatch = season.match(/Season (\d+)/);
  if (seasonMatch) {
    return seasonMatch[1].padStart(3, "0");
  }
  return season;
}


/**
 * Encodes an id to be used in a URL
 * @param {string} id The id to encode
 * @return {string} The encoded id
 */
export function encodeId(id: string): string {
  return id.replaceAll(".", "%2E")
      .replaceAll("$", "%24")
      .replaceAll("[", "%5B")
      .replaceAll("]", "%5D")
      .replaceAll("#", "%23")
      .replaceAll("/", "%2F");
}
