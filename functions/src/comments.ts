import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

export const createComment = functions.https.onCall(async (request) => {
  const data = request.data;
  if (!request.auth) {
    throw new functions.https.HttpsError("unauthenticated",
        "Must be authenticated to create a comment.");
  }

  const userId = request.auth.uid;
  const timestamp = new Date().toISOString();

  const userDoc = await admin.firestore().doc(`/users/${userId}`).get();
  const userName = userDoc.data()?.name || userId;
  const commentRef = admin.firestore()
      .collection(data.parent+"/comments").doc();
  await commentRef.set({
    comment: data.comment,
    uid: userId,
    userName: userName,
    timestamp: timestamp,
  });

  return {message: "Comment created successfully!"};
});

export const updateCommentNames = functions.firestore
    .onDocumentUpdated("/users/{userId}", async (event) => {
      const change = event.data;
      if (!change) {
        throw new Error("No change in user document");
      }
      const userId = event.params.userId;
      const newName = change.after.data().name;
      try {
        const commentsQuery = admin.firestore().collectionGroup("comments")
            .where("uid", "==", userId);
        const commentsSnapshot = await commentsQuery.get();

        commentsSnapshot.forEach(async (commentDoc) => {
          await commentDoc.ref.update({userName: newName});
        });
      } catch (e) {
        console.dir(e);
      }
    });
