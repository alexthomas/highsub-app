
import * as functions from "firebase-functions";

import express = require("express");
require("express-async-errors");

import {routes as articleRoutes} from "./article";
import {routes as authRoutes} from "./auth";
import {routes as catalogRoutes} from "./catalog";
import {routes as editorPickRoutes} from "./editorPick";
import {routes as episodeRoutes} from "./episode";
import {routes as publicSavedRoutes} from "./publicSavedSubtitles";
import {routes as showRoutes} from "./show";

const app = express();

app.use("/article", articleRoutes);
app.use("/auth", authRoutes);
app.use("/catalog", catalogRoutes);
app.use("/editor-pick", editorPickRoutes);
app.use("/episode", episodeRoutes);
app.use("/public-saved", publicSavedRoutes);
app.use("/show", showRoutes);


export const dataFunctions = functions.https.onRequest(app);
