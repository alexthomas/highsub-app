import express = require("express");
import * as admin from "firebase-admin";
import {COLLECTIONS} from "./types";

const {EDITOR_PICKS} = COLLECTIONS;
export const routes = express();

routes.get("/:key", async (req, res) => {
  const key = req.params.key;
  const snapshot = await admin.firestore().collection(EDITOR_PICKS).doc(key).get();
  res.json(snapshot.data());
});
