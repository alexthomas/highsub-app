import * as admin from "firebase-admin";
import express = require("express");
import {COLLECTIONS, Episode, type PublicEpisode} from "./types";
import {encodeId} from "./cleansingUtilities";

const {SHOWS, PUBLIC_EPISODES} = COLLECTIONS;
export const routes = express();

/**
 * Gets an episode from the database
 * @param {string} show
 * @param {string} season
 * @param {string} episode
 * @param {string} [id] The id of the episode, if not provided it will be generated
 * @return {Promise<Episode>} The episode object
 */
export async function getEpisode(show: string, season: string, episode: string, id?: string): Promise<Episode> {
  if (!id) {
    id = encodeId(`${show}-${season}-${episode}`);
  }
  const snapshot = await admin.firestore().collection(SHOWS)
      .doc(encodeId(show)).collection(PUBLIC_EPISODES).doc(id).get();
  return snapshot.data() as Episode;
}

routes.get("/:show/:id", async (req, res) => {
  const {show, id} = req.params;
  const episode = await getEpisode(show, "", "", id);
  res.json(episode);
});

routes.get("/:show/:id/context", async (req, res) => {
  const {show, id} = req.params;
  const episode = await getEpisode(show, "", "", id);
  const previousEpisode = await getContextEpisode(episode, "<", "desc");
  const nextEpisode = await getContextEpisode(episode, ">", "asc");
  return res.json({episode, previousEpisode, nextEpisode});
});


/**
 * Gets the previous or next episode in the series
 * @param {PublicEpisode} episode The episode to get the context of
 * @param {'>' | '<'} operator The operator to use in the query
 * @param {'asc' | 'desc'} order The order to use in the query
 * @return {Promise<PublicEpisode | null>} The episode object or null if not found
 */
async function getContextEpisode(episode: PublicEpisode, operator: ">" | "<", order: "asc" | "desc"):
  Promise<PublicEpisode | null> {
  const query = admin.firestore().collection(SHOWS).doc(episode.show)
      .collection(PUBLIC_EPISODES)
      .where("sortKey", operator, episode.sortKey)
      .orderBy("sortKey", order)
      .limit(1);
  const snapshot = await query.get();
  if (snapshot.empty) return null;
  const data = snapshot.docs[0].data() as PublicEpisode;
  return {...data, id: snapshot.docs[0].id} as PublicEpisode;
}
