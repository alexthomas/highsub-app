import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {Episode, SlimEpisode} from "./types";
import {getSortKey, mapEpisode} from "./cleansingUtilities";


export const updatePublicEpisodes = functions.firestore
    .onDocumentWritten("/shows/{show}/episodes/{episode}", async (event) => {
      const showId = event.params.show;
      // eslint-disable-next-line max-len
      const documentPath = `/shows/${showId}/publicEpisodes/${event.params.episode}`;
      if (!event.data?.after.exists) {
        await admin.firestore().doc(documentPath).delete();
      } else {
        const episodeData = event.data.after.data() as Episode;
        const publicEpisode = mapEpisode(episodeData);
        await admin.firestore().doc(documentPath).set(publicEpisode);
      }
    });

export const updateShowEpisodeCount = functions.firestore
    .onDocumentWritten("/shows/{show}/episodes/{episode}", async (event) => {
      const showId = event.params.show;
      const episodeCount = await admin.firestore()
          .collection(`shows/${showId}/episodes`)
          .count().get();

      // Update the show document with the new episode count
      await admin.firestore().doc(`/shows/${showId}`).update({
        indexedEpisodeCount: episodeCount.data().count,
      });
    });

type EpisodesCatalog = { episodes: SlimEpisode[], public: boolean };

export const updateShowCatalog = functions.firestore
    .onDocumentWritten("/shows/{show}/episodes/{episode}", async (event) => {
      const showId = event.params.show;
      const episodeId = event.params.episode;
      const documentReference = admin.firestore().collection("catalogs")
          .doc(`${showId}-episodes`);
      const catalogDoc = await documentReference.get();
      const catalog = catalogDoc.exists ?
        (catalogDoc.data() as EpisodesCatalog):
        {episodes: [], public: true};
      const episodes = catalog.episodes
          .filter((episode) => episode.id !== episodeId);
      if (event.data?.after.exists) {
        const episode = event.data.after.data() as Episode;
        const slimEpisode = getSlimEpisode(episode, episodeId);
        episodes.push(slimEpisode);
      } else {
        console.log(`Deleting episode ${episodeId}`);
      }
      catalog.episodes = episodes;
      await documentReference.set(catalog);
    });


/**
 * Get a slim version of an episode
 * @param {Episode} episode
 * @param {string} id
 * @return {SlimEpisode}
 */
function getSlimEpisode(episode: Episode, id: string): SlimEpisode {
  const {episode: episodeNumber, season, show, title, subtitleCount} = episode;
  const sortKey = getSortKey(episode);
  return {id, episode: episodeNumber, season, show, title, subtitleCount, sortKey};
}
