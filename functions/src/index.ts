import * as admin from "firebase-admin";

admin.initializeApp();


export * from "./episodeTriggers";
export * from "./showTriggers";
export * from "./comments";
export * from "./dataFunctions";
