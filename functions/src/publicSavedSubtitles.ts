import {COLLECTIONS} from "./types";
import * as admin from "firebase-admin";
import {getEpisode} from "./episode";
import express = require("express");
import createError = require("http-errors");

const {PUBLIC_SAVED, USERS, SAVED} = COLLECTIONS;
export const routes = express();

type PublicSavedSubtitle = {
  imageKey?: string,
  subtitle: {
    show: string,
    season: string,
    episode: string
  },
}

type PublicData = {
  owner: string,
  savedId: string
}

/**
 * Gets a public saved subtitle
 * @param {string} id
 * @return {Promise<PublicSavedSubtitle>} The public saved subtitle
 */
async function getPublicSavedSubtitle(id: string): Promise<PublicSavedSubtitle> {
  const snapshot = await admin.firestore().collection(PUBLIC_SAVED).doc(id).get();
  const publicData = snapshot.data() as PublicData;
  const savedSnapshot = await admin.firestore()
      .collection(USERS).doc(publicData.owner)
      .collection(SAVED).doc(publicData.savedId).get();
  return savedSnapshot.data() as PublicSavedSubtitle;
}

/**
 * Gets the public saved data
 * @param {string} id The id of the public saved subtitle
 * @return {Promise<unknown>} The public saved data
 */
async function getPublicSavedData(id: string): Promise<unknown> {
  const savedSubtitle = await getPublicSavedSubtitle(id);
  if (!savedSubtitle) {
    throw createError(404, "Not Found");
  }
  const s = savedSubtitle.subtitle;
  const episode = await getEpisode(s.show, s.season, s.episode);
  const imageUrl = savedSubtitle.imageKey ?
    admin.storage().bucket("aniclips-fbfa3.appspot.com")
        .file(savedSubtitle.imageKey).publicUrl() :
    null;
  await admin.firestore().collection(PUBLIC_SAVED).doc(id).update({viewCount: admin.firestore.FieldValue.increment(1)});
  return {
    subtitle: savedSubtitle,
    episode,
    imageUrl,
  };
}

routes.get("/:id", async (req, res) => {
  const id = req.params.id;
  const data = await getPublicSavedData(id);
  res.json(data);
});
