import express = require("express");
import * as admin from "firebase-admin";
import {COLLECTIONS} from "./types";
import {encodeId} from "./cleansingUtilities";

const {SHOWS} = COLLECTIONS;
export const routes = express();

routes.get("/anime", async (req, res) => {
  const snapshot = await admin.firestore().collection(SHOWS)
      .where("tags", "array-contains", "anime").where("indexedEpisodeCount", ">", 0).limit(500).get();
  const shows = snapshot.docs.map((doc) => doc.data());
  res.json(shows);
});

routes.get("/:show", async (req, res) => {
  const show = req.params.show;
  const snapshot = await admin.firestore().collection(SHOWS).doc(encodeId(show)).get();
  res.json(snapshot.data());
});
