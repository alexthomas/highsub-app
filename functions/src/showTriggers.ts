import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import type {Show, SlimShow} from "./types";

export const updateShowsCatalog = functions.firestore
    .onDocumentWritten("/shows/{show}", async (event) => {
      if (!event.data) {
        throw new Error("No data");
      }
      const after = event.data.after;
      const beforeShow = after.data() as Show | undefined;
      const afterShow = after.data() as Show | undefined;
      const showId = event.params.show;
      await updateShowsCatalogItem("adminShows", showId, beforeShow, afterShow);
      await updateShowsCatalogItem("shows", showId,
        (beforeShow?.indexedEpisodeCount??0)>0?beforeShow:undefined,
        (afterShow?.indexedEpisodeCount??0)>0?afterShow:undefined);
    });

/**
 * Update the shows catalog
 * @param {string}key the key of the catalog
 * @param {string}showId the show id
 * @param {Show|undefined}beforeShow the show before the change
 * @param {show|undefined}afterShow the show after the change
 */
async function updateShowsCatalogItem(key: string, showId: string,
    beforeShow: Show | undefined, afterShow: Show | undefined) {
  const catalogDocRef = admin.firestore().collection("catalogs")
      .doc(key);
  if (!beforeShow && afterShow) {
    await catalogDocRef.update({
      shows: admin.firestore.FieldValue.arrayUnion(getSlimShow(afterShow)),
    });
  } else {
    const catalog = (await catalogDocRef.get())
        .data() as { shows: SlimShow[] };
    const shows = catalog.shows
        .filter((show) => show.show !== showId);
    if (afterShow) {
      shows.push(getSlimShow(afterShow));
    } // else the show was deleted
    await catalogDocRef.update({shows});
  }
}

export const createShowCatalog = functions.firestore
    .onDocumentCreated("/shows/{show}", async (event) => {
      const showCatalog = {public: true, episodes: []};
      await admin.firestore()
          .doc(`catalogs/${event.params.show}-episodes`)
          .set(showCatalog);
    });

export const deleteShowCatalog = functions.firestore
    .onDocumentDeleted("/shows/{show}", async (event) => {
      await admin.firestore()
          .doc(`catalogs/${event.params.show}-episodes`)
          .delete();
    });


/**
 * Get a slim version of a show
 * @param {Show} show
 * @return {SlimShow}
 */
function getSlimShow(show: Show): SlimShow {
  return {
    show: show.show,
    summary: show.summary,
    tags: show.tags,
    indexedEpisodeCount: show.indexedEpisodeCount,
  };
}

