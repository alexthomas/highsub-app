export type PublicEpisode ={
  show: string,
  season: string,
  episode: string,
  title: string,
  summary?: string,
  studio?: string,
  endingStart?: number,
  endingEnd?: number,
  openingStart?: number,
  openingEnd?: number
  originallyAvailableAt?: string,
  previewStart?: number,
  previewEnd?: number,
  prologueStart?: number,
  prologueEnd?: number,
  subtitleCount?: number,
  height?: number,
  width?: number,
  sortKey?: string
}

export type Episode = {
  show: string,
  season: string,
  episode: string,
  title: string,
  summary: string,
  studio: string,
  endingStart: number,
  endingEnd: number,
  englishSubtitles: string,
  japaneseSubtitles: string,
  openingStart: number,
  openingEnd: number
  originallyAvailableAt: string,
  previewStart: number,
  previewEnd: number,
  prologueStart: number,
  prologueEnd: number,
  scaled150pVideo: string,
  scaled150pVideoBytes: number,
  scaled480pVideo: string,
  scaled480pVideoBytes: number,
  sourceVideo: string,
  sourceVideoBytes: number,
  subtitleCount: number,
  height: number,
  width: number,
}

export type Show = {
  anime:boolean,
  indexedEpisodeCount: number,
  kitsuAnimeId: number,
  plexKey: string,
  show: string,
  summary: string,
  tags: string[],
  tmdbEpisodeGroupId: string,
  tmdbShowId: string,
  year: number
}


export type SlimShow = Pick<Show, "show"|"summary"|"tags"|
  "indexedEpisodeCount">;
export type SlimEpisode = Pick<PublicEpisode, "show"|"season"|"episode"|
  "subtitleCount"|"title"|"sortKey"> & {id: string}

export const COLLECTIONS = {
  SHOWS: "shows",
  USERS: "users",
  INDEXING_STATUS: "indexing-status",
  PUBLIC_EPISODES: "publicEpisodes",
  EPISODES: "episodes",
  CATALOGS: "catalogs",
  ARTICLES: "articles",
  COLLECTIONS: "collections",
  COLLECTION_CLIPS: "clips",
  SAVED: "savedSubtitles",
  LIKED: "likedSubtitles",
  EDITOR_PICKS: "editorPicks",
  COMMENTS: "comments",
  GUESSES: "guesses",
  MEMORY_RESULTS: "memoryResults",
  PUBLIC_SAVED: "publicSaved",
};
