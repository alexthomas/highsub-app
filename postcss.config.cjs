const tailwindcss = require("tailwindcss");
const autoprefixer = require("autoprefixer");
const tailwindNested = require("@tailwindcss/nesting");
const tailwindForms = require("@tailwindcss/forms");
const tailwindTypography = require("@tailwindcss/typography");

const config = {
	plugins: [
		tailwindNested,
		tailwindForms,
		tailwindTypography,

		//Some plugins, like tailwindcss/nesting, need to run before Tailwind,
		tailwindcss(),
		//But others, like autoprefixer, need to run after,
		autoprefixer,
	],
};

module.exports = config;