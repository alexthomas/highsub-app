import { ANIME_QA_HOST } from '$lib/constants';
import type {
	EpisodeContext,
	EpisodeSummary
} from '../routes/(app)/admin/shows/[showId]/episodes/[episodeId]/description/types';
import { type Message, nanoid } from 'ai';
import { datadogLogs } from '@datadog/browser-logs';
import type { Article as ArticleType, Content, Prompt } from '../routes/(app)/article/types';
import type { Reference } from '$lib/types';
import { useChat } from '@ai-sdk/svelte';


export function getChat(initialMessages: undefined | Message[] = undefined,noTools:boolean=false) {
	return useChat({
		api: noTools?'/api/ai/chat?noTools=true':'/api/ai/chat',
		initialMessages,
		experimental_onToolCall: async (chatMessages, toolCalls) => {
			let handledFunction = false;
			for (const tool of toolCalls) {
				const { name, arguments: args } = tool.function;
				try {
					if (name === 'getEpisodeSummaries') {
						const summaries = await getEpisodeSummaries(args);
						chatMessages.push({
							id: nanoid(),
							role: 'tool',
							tool_call_id: tool.id,
							content: summaries.join('\n'),
							name
						});
					} else if (name === 'getEpisodeSummary') {
						const episodeSummary = await getEpisodeSummary(args);
						if (!episodeSummary) {
							chatMessages.push({
								id: nanoid(),
								role: 'tool',
								tool_call_id: tool.id,
								content: `No summary found`,
								name
							});
						} else {
							const { show, season, episode, title, generatedSummary } = episodeSummary;
							const summary = JSON.stringify({ show, season, episode, title, summary: generatedSummary });
							chatMessages.push({ id: nanoid(), role: 'tool', tool_call_id: tool.id, content: summary, name });
						}
					} else {
						console.log('unhandled tool call', tool);
					}
				} catch (e) {
					chatMessages.push({
						id: nanoid(),
						role: 'tool',
						tool_call_id: tool.id,
						content: `Error: ${e.message}`,
						name
					});
					datadogLogs.logger.error('Failed to handle tool call', { tool, error: e });
				}
				handledFunction = true;
			}
			if (handledFunction) {
				return { messages: chatMessages };
			}
		}
	});
}

export async function getObject(prompt: string, schemaName: 'advice') {
  const response = await fetch('/api/ai/object', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ prompt, schemaName })
  });
  if (!response.ok) {
    throw new Error('Failed to get object');
  }
  return response.json();
}

export async function getEpisodeSummaries(args: string) {
	const response = await fetch(`${ANIME_QA_HOST}/v1/answer/context`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: args
	});
	if (!response.ok) {
		throw new Error('Failed to get episode summaries');
	}
	return response.json();
}

export type ParsedEpisodeArgs = { show: string, season: string, episode: string };

export async function getEpisodeSummary(args: string | undefined = undefined, episodeId: ParsedEpisodeArgs | undefined = undefined): Promise<EpisodeSummary | undefined> {
	if (!args && !episodeId) {
		throw new Error('No arguments provided');
	}
	const id = episodeId ? getSummaryId(episodeId) : getSummaryId(JSON.parse(args!));
	const response = await fetch(`${ANIME_QA_HOST}/v1/summary/${id}`);
	if (!response.ok) {
		throw new Error('Failed to get episode summary');
	}
	if (response.headers.get('content-length') === '0')
		return undefined;
	return await response.json();
}

export async function getEpisodeContextItems(args: string | undefined = undefined, episodeId: ParsedEpisodeArgs | undefined = undefined): Promise<EpisodeContext|undefined> {
	if (!args && !episodeId) {
		throw new Error('No arguments provided');
	}
	const id = episodeId ? getSummaryId(episodeId) : getSummaryId(JSON.parse(args!));
	const response = await fetch(`${ANIME_QA_HOST}/v1/episode-context/episode/${id}/itemized`);
	if (!response.ok) {
		throw new Error('Failed to get episode context');
	}
	if (response.headers.get('content-length') === '0')
		return undefined;
	return await response.json();
}

function getSummaryId(episode: ParsedEpisodeArgs) {
	return encodeURIComponent(`${episode.show}|${episode.season}|${episode.episode}`);
}

export const ARTICLE_FORMAT = `output in a custom markdown-like format with @id@,$title$,%tags%,#headers,paragraphs, and [image placeholders]. Image placeholders are optional. The id should be url safe and less than 100 characters but detailed enough to be unique.

example:
	@name-of-the-show-article-thesis@
	$Article Title$
	%anime, review, analysis%

	#Introduction
	The show is about...
	
	#Plot point
	In this plot point...
	[Image showing the plot point]`

export function parseArticle(messages:Message[],publishInDays:number,prompt:Prompt): ArticleType {
	const messageContent = messages.filter(m=>m.role==='assistant' && !m.tool_calls)
		.map(m=>m.content).join('\n');
	const lines = messageContent.split('\n');
	let id = '', title = '', tags: string[] = [], articleContent: Content[] = [];
	for (let i = 0; i < lines.length; i++) {
		const line = lines[i].trim();
		let content: Content | undefined = undefined;
		if (line.startsWith('@')) {
			id = line.replaceAll('@', '').trim();
		} else if (line.startsWith('$')) {
			title = line.replaceAll('$', '').trim();
		} else if (line.startsWith('%')) {
			tags = line.replace('%', '').replace('%', '').split(',').map(t => t.trim());
		} else if (line.startsWith('#')) {
			content = { type: 'header', text: line.replace('#', '').trim() };
		} else if (line.startsWith('[')) {
			content = { type: 'imagePlaceholder', imageDescription: line.replace('[', '').replace(']', '').trim() };
		} else if (line.trim().length > 0) {
			content = { type: 'paragraph', text: line };
		}
		if (content) {
			articleContent.push(content);
		}
	}
	return {
		id,
		title,
		tags,
		content: articleContent,
		publishedUtc: Date.now() + (publishInDays * 24 * 60 * 60 * 1000),
		prompt,
		messages,
		references: parseReferences(messages)
	};
}

function parseReferences(messages:Message[]):Reference[]{
	return messages.filter(m=>m.role==='tool').flatMap(m=>m.content.split("\n"))
		.map(l=>JSON.parse(l));
}

