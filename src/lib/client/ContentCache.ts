import { getAuthHeader } from '$lib/client/auth';

const CACHE_KEY_PREFIX = 'content-cache-';
const MAX_RUNNERS = 3;
const runners: object[] = [];
const queue: { url: string, resolve: (response: Response) => void, reject: () => void }[] = [];

function getCacheKey() {
  const dateHour = new Date().toISOString().slice(0, 13);
  return `${CACHE_KEY_PREFIX}${dateHour}`;
}

export async function add(url: string, priority = true) {
  const cache = await caches.open(getCacheKey());
  if (await cache.match(new URL(url))) return;
  const fetchPromise: Promise<Response> = new Promise((resolve, reject) => {
    if (priority)
      queue.unshift({ url, resolve, reject });
    else
      queue.push({ url, resolve, reject });
  });
  if (runners.length < MAX_RUNNERS) startRunner().then();
  const response = await fetchPromise;
  if (response.status === 200) await cache.put(url, response.clone());
}

async function startRunner() {
  const runner = {};
  runners.push(runner);
  while (queue.length > 0) {
    const { url, resolve, reject } = queue.shift()!;
    try {
      const headers = await getAuthHeader();
      const response = await fetch(url, { headers });
      resolve(response);
    } catch (e) {
      reject();
    }
  }
  runners.splice(runners.indexOf(runner), 1);
}

export async function get(request: Request) {
  const cache = await caches.open(getCacheKey());
  const cacheResponse = await cache.match(request);
  if (cacheResponse) return cacheResponse;
  const url = new URL(request.url);
  if (url.searchParams.has('countHit')) {
    url.searchParams.delete('countHit');
    const hitlessResponse = await cache.match(url.toString());
    if (hitlessResponse) {
      const headers = await getAuthHeader();
      fetch(request, { headers }).then(); //fire and forget just so that the hit gets counted
      return hitlessResponse;
    }
  }
  const headers = await getAuthHeader();
  const response = await fetch(request, { headers });
  if (response.status === 200) await cache.put(request, response.clone());
  return response;
}

async function deleteOldCaches() {
  if (window.caches && caches.keys) {
    const cacheKey = getCacheKey();
    for (const key of await caches.keys()) {
      if (key.startsWith(CACHE_KEY_PREFIX) && key !== cacheKey) await caches.delete(key);
    }
  }
}

setInterval(deleteOldCaches, 1000 * 60 * 60); //delete old caches every hour