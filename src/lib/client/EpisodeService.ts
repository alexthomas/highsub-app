import type { Episode, Subtitle } from '$lib/types';
import { doc, getDoc, getFirestore } from '@firebase/firestore';
import { COLLECTIONS, encodeId } from '$lib/firebase';
import { getToken } from '$lib/client/auth';
import { SERVER_URL } from '$lib/constants';

export async function getEpisode(subtitle: Pick<Subtitle, 'show' | 'season' | 'episode'>): Promise<Episode | undefined> {
  const localId = encodeId(`${subtitle.show}-${subtitle.season}-${subtitle.episode}`);
  const episode = await getDoc(doc(getFirestore(), COLLECTIONS.SHOWS, encodeId(subtitle.show), COLLECTIONS.PUBLIC_EPISODES, encodeId(localId)));
  return episode.data() as Episode;
}

export async function episodeAction(type: string, episode: Pick<Episode, 'show' | 'season' | 'episode'>): Promise<Response> {
  const token = await getToken();
  return await fetch(`${SERVER_URL}/v1/episode/${encodeURIComponent(episode.show)}-${episode.season}-${episode.episode}/${type}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  });
}

export type PlexOpening = {
  show: string;
  season: number;
  episode: number;
  id: number;
  marker_type: string;
  extra_data: string;
  start: number;
  end: number;
}
export async function uploadOpenings(openings:PlexOpening[]){
  const token = await getToken();
  return await fetch(`${SERVER_URL}/v1/plex/openings`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(openings)
  });
}