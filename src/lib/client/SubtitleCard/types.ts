import type { ContextualSubtitle, Subtitle } from '$lib/types';
import { applyCountHit, applyHeight } from '$lib/utilities';

export type SubtitleCardConfiguration = {
  autoplay: boolean,
  muted: boolean,
  mediaType: 'video' | 'image' | 'auto',
  aspectRatio: string | number,
  mediaContainerAspectRatio: string | number,
  containerClasses: string,
  viewportContainer: string | undefined,
  cardClasses: string,
  intersectionMargin: string,
  countHit: boolean,
  isSpoiler: boolean,
  sourceSize: boolean,
  clickAction: 'drawer' | 'link'
}

export const DEFAULT_CONFIGURATION: SubtitleCardConfiguration = {
  autoplay: false,
  muted: true,
  mediaType: 'auto',
  aspectRatio: '16/9',
  mediaContainerAspectRatio: '16/9',
  containerClasses: 'rounded-lg',
  cardClasses: 'rounded-lg inline-block',
  viewportContainer: 'page',
  intersectionMargin: '-200px 0px -300px 0px',
  countHit: false,
  isSpoiler: false,
  sourceSize: false,
  clickAction: 'drawer'
};

export interface MediaDataExtractor<T> {
  getVideoUrl: (data: T, height: number|null, countHit: boolean) => string;
  getImageUrl: (data: T, height: number|null, countHit: boolean) => string;
  getTags: (data: T) => string[];
  getId: (data: T) => string;
  getSubtitleLink: (data: T) => string;
  getSubtitleKey: (data: T) => { show: string, season: string, episode: string, subtitleNumber: number };
  getTiming: (data:T)=> {startTime:number,endTime:number};
  getText: (data:T) => string;
  getShow: (data:T) => string;
}

export const SubtitleDataExtractor: MediaDataExtractor<Subtitle> = {
  getVideoUrl(data: Subtitle, height: number|null, countHit: boolean): string {
    const url = height?applyHeight(data.videoUrl, height):data.videoUrl;
    return  applyCountHit(url,countHit).toString();
  },
  getImageUrl(data: Subtitle, height: number|null, countHit: boolean): string {
    const url = height?applyHeight(data.pngUrl, height):data.pngUrl;
    return  applyCountHit(url,countHit).toString();
  },
  getTags(data: Subtitle): string[] {
    const tags = [];
    if (data.tags)
      tags.push(...data.tags);
    if (data.parentTags)
      tags.push(...data.parentTags);
    return tags;
  },
  getId(data: Subtitle): string {
    return data.id;
  },
  getSubtitleLink(data: Subtitle): string {
    return window.location.origin + `/subtitle/${data.id}`;
  },
  getSubtitleKey(data: Subtitle): { show: string, season: string, episode: string, subtitleNumber: number } {
    const { show, season, episode, subtitleNumber } = data;
    return { show, season, episode, subtitleNumber };
  },
  getTiming(data:Subtitle):{startTime:number,endTime:number}{
    return {startTime:data.startTime,endTime:data.endTime};
  },
  getText(data:Subtitle):string{
    return data.text;
  },
  getShow(data:Subtitle):string{
    return data.show;
  }
};

export const ContextSubtitleDataExtractor: MediaDataExtractor<ContextualSubtitle> = {
  getVideoUrl(data: ContextualSubtitle, height: number|null, countHit: boolean): string {
    const url = new URL(SubtitleDataExtractor.getVideoUrl(data.subtitle,height,countHit));
    if(data.context.plusMs)
      url.searchParams.set("plus",data.context.plusMs.toString());
    if(data.context.minusMs)
      url.searchParams.set("minus",data.context.minusMs.toString());
    return url.toString();
  },
  getImageUrl(data: ContextualSubtitle, height: number|null, countHit: boolean): string {
    return SubtitleDataExtractor.getImageUrl(data.subtitle,height,countHit);
  },
  getTags(data: ContextualSubtitle): string[] {
    return SubtitleDataExtractor.getTags(data.subtitle);
  },
  getId(data: ContextualSubtitle): string {
    return SubtitleDataExtractor.getId(data.subtitle);
  },
  getSubtitleLink(data: ContextualSubtitle): string {
   const url =  new URL(SubtitleDataExtractor.getSubtitleLink(data.subtitle));
   if(data.context.previousCount)
     url.searchParams.set("previous",data.context.previousCount.toString());
    if(data.context.nextCount)
      url.searchParams.set("next",data.context.nextCount.toString());
    return url.toString();
  },
  getSubtitleKey(data: ContextualSubtitle): { show: string, season: string, episode: string, subtitleNumber: number } {
    return SubtitleDataExtractor.getSubtitleKey(data.subtitle);
  },
  getTiming(data:ContextualSubtitle):{startTime:number,endTime:number}{
    const startTime = data.context.subtitles.map(s=>s.startTime).reduce((a,b)=>Math.min(a,b));
    const endTime = data.context.subtitles.map(s=>s.endTime).reduce((a,b)=>Math.max(a,b));
    return {startTime,endTime};
  },
  getText(data:ContextualSubtitle):string {
    return data.context.subtitles.map(s=>s.text).join(' ');
  },
  getShow(data:ContextualSubtitle):string{
    return data.context.subtitles[0].show;
  }
}

export class SubtitleData<T>  {
  constructor(private data: T,private extractor:MediaDataExtractor<T>) {
  }

  public getVideoUrl(height: number|null, countHit: boolean): string {
    return this.extractor.getVideoUrl(this.data, height, countHit);
  }

  public getImageUrl(height: number|null, countHit: boolean): string {
    return this.extractor.getImageUrl(this.data, height, countHit);
  }

  public getFirstFameUrl(height:number|null,countHit:boolean):string{
    const url = new URL(this.getImageUrl(height,countHit));
    url.searchParams.set('firstFame','true');
    return url.toString();
  }

  public getTags(): string[] {
    return this.extractor.getTags(this.data);
  }

  public getId(): string {
    return this.extractor.getId(this.data);
  }

  public getSubtitleLink(): string {
    return this.extractor.getSubtitleLink(this.data);
  }

  public getSubtitleKey(): { show: string, season: string, episode: string, subtitleNumber: number } {
    return this.extractor.getSubtitleKey(this.data);
  }

  public getTiming():{startTime:number,endTime:number}{
    return this.extractor.getTiming(this.data);
  }

  public getText():string{
    return this.extractor.getText(this.data);
  }

  public getSubtitle():T{
    return this.data;
  }

  public getShow():string{
    return this.extractor.getShow(this.data);
  }
}