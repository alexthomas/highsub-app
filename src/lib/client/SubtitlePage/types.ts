export type ActorRole = { actor: string, show: string, score: number, role: string, character: string }
export type SameActor = { actorName: string, originalShowCharacters: Array<ActorRole>, originalShow: string, otherShowCharacters: Array<ActorRole>, otherShow: string, score: number }
