import { Account, Avatars, Client, Databases, type Models, Query, Storage, Teams } from "appwrite";
import type { Readable, Writable } from "svelte/store";
import { derived, writable } from "svelte/store";
import { datadogRum } from "@datadog/browser-rum";
import { unleash } from "../unleash";
import { PUBLIC_APPWRITE_PROJECT } from "$env/static/public";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import type Team = Models.Team;
import { SERVER_URL } from "$lib/constants";
import type { SpoilerConfig } from '$lib/types';


export const ENDPOINT = "https://appwrite.alexk8s.com/v1";
export const PROJECT = PUBLIC_APPWRITE_PROJECT;
export const client = new Client()
  .setEndpoint(ENDPOINT)
  .setProject(PROJECT);
export const account: Writable<Models.User<Preferences> | undefined> = writable();
export const accountClient = new Account(client);
export type Preferences = {spoilerConfigs?: SpoilerConfig[]};

export const avatarsClient = new Avatars(client);
export const teamsClient = new Teams(client);
export const storageClient = new Storage(client);







