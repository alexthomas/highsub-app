import { app } from './firebase';
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut as firebaseSignOut,
  type User,
  type UserCredential,
  signInWithPopup,
  GoogleAuthProvider
} from 'firebase/auth';
import { derived, type Readable, writable } from 'svelte/store';
import { datadogRum } from '@datadog/browser-rum';
import { doc, getFirestore, onSnapshot } from '@firebase/firestore';
import { COLLECTIONS } from '$lib/firebase';
import { setDoc } from 'firebase/firestore';

export const user = writable<User | null>();
export const isAdmin: Readable<boolean> = derived(user, (u, finish) => {
  if (!u)
    return finish(false);
  u.getIdToken().then(token => parseJwt(token)).then(jwt => {
    console.log(jwt);
    finish(jwt['admin'] === true);
  });
});
user.subscribe(u => console.log('user', u));

const auth = getAuth(app);
auth.onAuthStateChanged(u => user.set(u));
user.subscribe(u => {
  const userData = u?.providerData || [];
  const id = userData.map(d => d.uid).filter(d => !!d)[0];
  const email = userData.map(d => d.email).filter(d => !!d)[0]!;
  const name = userData.map(d => d.displayName).filter(d => !!d)[0]!;
  const emailVerified = u?.emailVerified;
  const isAnonymous = u?.isAnonymous;
  datadogRum.setUser({ id, email, name, userData, emailVerified, isAnonymous });
});
auth.onIdTokenChanged(async u => {
  if (u) {
    const token = await u.getIdToken();
    await fetch('/api/token', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ token })
    });
  }
});

export async function authenticate(email: string, password: string): Promise<UserCredential> {
  return await signInWithEmailAndPassword(auth, email, password);
}


export async function googleSignIn(): Promise<UserCredential> {
  const provider = new GoogleAuthProvider();
  return await signInWithPopup(auth, provider);
}

export async function createUser(email: string, password: string): Promise<UserCredential> {
  try {
    return await createUserWithEmailAndPassword(auth, email, password);
  } catch (e) {
    // @ts-ignore
    const { code, message } = e;
    throw new Error(message);
  }
}

export function signOut(): Promise<void> {
  return firebaseSignOut(auth);
}

export async function getToken(): Promise<string> {
  if (!auth.currentUser) {
    throw new Error('Not authenticated');
  }
  return await auth.currentUser.getIdToken();
}

export function isAuthenticated(): boolean {
  return !!auth.currentUser;
}

export async function getAuthHeader(): Promise<{Authorization: string}|undefined> {
  if (!auth.currentUser) {
    return undefined;
  }
  return { Authorization: `Bearer ${await auth.currentUser.getIdToken()}` };
}

function parseJwt(token: string) {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
}

export type UserProfile = { name?: string }
let profileKey: string | null = null;
let profile: Readable<UserProfile> | null = null;

export function getProfile(): Readable<UserProfile> {
  if (!isAuthenticated())
    throw new Error('Not authenticated');
  if (!profile || profileKey !== auth.currentUser?.uid) {
    const writableProfile = writable<UserProfile>({});
    onSnapshot(doc(getFirestore(app), COLLECTIONS.USERS, auth.currentUser!.uid), d => {
      if(!d.exists()){
        setDoc(doc(getFirestore(app), COLLECTIONS.USERS, auth.currentUser!.uid), {}).then();
      } else{
        writableProfile.set(d.data() || {});
      }
    });
    profile = writableProfile;
    profileKey = auth.currentUser!.uid;
  }
  return profile;

}
