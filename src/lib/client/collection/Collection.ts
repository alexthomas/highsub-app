import type {
  ModalComponent,
  ModalSettings,
  ModalStore, ToastStore
} from '@skeletonlabs/skeleton';
import CreateNew from './CreateNew.svelte';
import type { Subtitle } from '$lib/types';
import { writable } from 'svelte/store';
import { collection, doc, getDocs, getFirestore, query, where } from '@firebase/firestore';
import { app } from '$lib/client/firebase';
import { COLLECTIONS } from '$lib/firebase';
import { setDoc } from 'firebase/firestore';


export type Collection = {
  name: string,
  createdBy: string,
  coverVideoUrl?: string,
  coverGiftUrl?: string,
  id: string,
}

export type CollectionClip = Subtitle & {
  addedBy: string,
  order: number
}

export const subtitleStore = writable<Subtitle>(undefined);

export async function getCollectionsPromise(account: { uid: string }): Promise<Array<Collection>> {
  const collectionQuery = query(collection(getFirestore(app), COLLECTIONS.COLLECTIONS), where('createdBy', '==', account.uid));
  const querySnapshot = await getDocs(collectionQuery);
  return querySnapshot.docs.map(doc => ({...doc.data(),id:doc.id}) as Collection);
}

export async function addToCollection(collection: Collection, clip: Subtitle, accountId: string, toastStore: ToastStore, modalStore: ModalStore) {
  const data = { ...clip, addedBy: accountId };
  try {
    const docRef = doc(getFirestore(app), COLLECTIONS.COLLECTIONS, collection.id, COLLECTIONS.COLLECTION_CLIPS, clip.id);
    await setDoc(docRef, data);
    toastStore.trigger({ message: `Added Clip to ${collection.name}`, background: 'bg-success-500' });
  } catch (e:unknown|string|{message:string}) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
    toastStore.trigger({ message: e?.message||e, background: 'bg-danger-500' });
  }
  modalStore.clear();
}

export function createCollection(clip: Subtitle, accountId: string, modalStore: ModalStore, toastStore: ToastStore) {
  const modalComponent: ModalComponent = {
    ref: CreateNew
  };
  const modalSettings: ModalSettings = {
    type: 'component',
    component: modalComponent,
    response(r) {
      if (r)
        addToCollection(r, clip, accountId, toastStore, modalStore).then();
    }
  };
  modalStore.clear();
  modalStore.trigger(modalSettings);
}