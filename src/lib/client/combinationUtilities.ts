import { SERVER_URL } from "$lib/constants";
import type { Subtitle } from '$lib/types';

//todo delete this function
export function stitch(baseUrl:string,subtitle:Subtitle,subtitles:Subtitle[]) {
  if (subtitles.length > 1) {
    const minus = subtitle.startTime - subtitles[0].startTime;
    const plus = subtitles[subtitles.length - 1].endTime - subtitle.endTime;
    const url = new URL(baseUrl);
    if (plus > 0)
      url.searchParams.set("plus", String(plus));
    if (minus > 0)
      url.searchParams.set("minus", String(minus));
    return url.toString();
  }
  return baseUrl;
}

export function getCombinedImageUrl(subtitle:Subtitle,subtitles:Subtitle[]) {
  const domain = subtitle.domain || SERVER_URL;
  const url  = `${domain}/v1/content/subtitle/combined/${subtitle.show}-${subtitle.season}-${subtitle.episode}.png?${subtitles.map(x => "subtitle=" + x.subtitleNumber).join("&")}`;
  return encodeURI(url)
}
