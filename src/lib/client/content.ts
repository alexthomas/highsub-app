import type { Episode, Show, Subtitle } from '$lib/types';
import { getFirestore, collection, query, where, getDocs, doc, getDoc, limit } from '@firebase/firestore';
import { COLLECTIONS, encodeId } from '$lib/firebase';
import { SERVER_URL } from '$lib/constants';
import {app} from '$lib/client/firebase';

export async function getShows(): Promise<Show[]> {
  const showsRef = collection(getFirestore(app), COLLECTIONS.SHOWS);
  const q = query(showsRef, where('indexedEpisodeCount', '>', 0), limit(500));
  const results = await getDocs(q);
  return results.docs.map(d => d.data() as Show);
}

export async function getShow({ id, name }: { id?: string, name?: string }): Promise<Show | undefined> {
  const key = id || name;
  if (!key) {
    return undefined;
  }
  const docRef = doc(getFirestore(), COLLECTIONS.SHOWS, encodeId(key));
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    return docSnap.data() as Show;
  } else {
    return undefined;
  }
}

export async function getPublicEpisodes(show: string): Promise<Episode[]> {
  const episodesRef = collection(getFirestore(), COLLECTIONS.SHOWS, encodeId(show), COLLECTIONS.PUBLIC_EPISODES);
  const q = query(episodesRef, limit(2000));
  const results = await getDocs(q);
  return results.docs.map(d => ({ ...(d.data()), id: d.id } as Episode));
}

export function showPosterUrl(show: {show:string}) {
  return `${SERVER_URL}/v1/content/poster/${encodeURIComponent(show.show)}.png`;
}

export async function getRandomSubtitles(count: number = 1): Promise<Subtitle[]> {
  const response = await fetch(`${SERVER_URL}/v2/subtitle/random?count=${count}`);
  return await response.json();
}

export async function getRandomShowSubtitles(show:string,count:number=1):Promise<Subtitle[]>{
  const response = await fetch(`${SERVER_URL}/v2/subtitle/show/${encodeURIComponent(show)}/random?count=${count}`);
  return await response.json();
}

