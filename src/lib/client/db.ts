import Dexie, { type IndexableType, type Table } from 'dexie';
import type { SearchCriteria } from '../../routes/(app)/search/stores';
import type { QuizQuestion, Subtitle as SubtitleType, Subtitle } from '$lib/types';

export type SearchHistoryRecord = {
	id?: number,
	searchCriteria: SearchCriteria,
	searchDate: Date,
	searchUrl: string,
	parseVersion: string,
	parsedQuery?: ParsedQuery;
}

export type ParsedQuery = {
	q?: string,
	text?: string,
	romaji?: string,
	kanji?: string,
	show?: string,
	season?: string,
	episode?: string,
	tags?: string[],
	name?: string,
	blacklistShow?: string[],
	size?: number,
	page?: number,
};

export type ClipPlay = {
	id: string,
	show: string,
	season: string,
	episode: string,
	subtitleNumber: number,
	url: string,
	playTime: number,
}

export type SearchState = {
	id: string,
	clips: SubtitleType[],
	slice: { start: number, end: number },
	page: number,
	hasMore: boolean,
	createTime: number,
	updateTime: number,
	scrollPosition: number,
};

export type AnsweredQuizQuestion = QuizQuestion & {selected:string,attemptCount:number}
export class HighsubDatabase extends Dexie {
	searchHistory!: Table<SearchHistoryRecord>;
	clipPlay!: Table<ClipPlay>;
	searchState!: Table<SearchState>;
	answeredQuizQuestion!: Table<AnsweredQuizQuestion>;

	constructor() {
		super('highsub');
		this.version(10).stores({
			searchHistory: '++id,searchDate,parseVersion',
			clipPlay: 'id,show,season,episode,subtitleNumber,playTime',
			searchState: 'id',
			answeredQuizQuestion: 'id'
		});
	}
}

let db: HighsubDatabase;

export function getDb() {
	if (!db) {
		db = new HighsubDatabase();
	}
	return db;
}