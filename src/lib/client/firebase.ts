import { initializeApp } from 'firebase/app';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCDcKGZtWRblPU6Tkua1myAojH2mYnxELQ',
  authDomain: 'aniclips-fbfa3.firebaseapp.com',
  projectId: 'aniclips-fbfa3',
  storageBucket: 'aniclips-fbfa3.appspot.com',
  messagingSenderId: '1028475156895',
  appId: '1:1028475156895:web:ff85f8091c0b2eebd1e5aa'
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
