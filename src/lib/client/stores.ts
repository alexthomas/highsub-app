import type { Readable } from "svelte/store";
import { writable } from "svelte/store";
import { browser } from "$app/environment";


const localIsSmallScreen = writable(false);
const localIsMobile = writable(false);
const localIsWebkit = writable(false);

if (browser) {
  const mediaQuery = window.matchMedia("(max-width: 1024px)");
  localIsSmallScreen.set(mediaQuery.matches);
  mediaQuery.addEventListener("change", (e) => {
    localIsSmallScreen.set(e.matches);
  });
  if (navigator && navigator.userAgent) {
    localIsMobile.set(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
    localIsWebkit.set(/WebKit/i.test(navigator.userAgent));
  }
}

export const isSmallScreen: Readable<boolean> = localIsSmallScreen;
export const isMobile: Readable<boolean> = localIsMobile;
export const isWebkit: Readable<boolean> = localIsWebkit;
export const searchInput = writable("");