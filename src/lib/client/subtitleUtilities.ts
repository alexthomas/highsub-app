import type { Subtitle, SubtitleContext } from '$lib/types';
import { add as contentAdd } from '$lib/client/ContentCache';
import { SERVER_URL } from '$lib/constants';
import type { ModalComponent, ModalSettings, ModalStore } from '@skeletonlabs/skeleton';
import ShareModal from '../../routes/(app)/search/ShareModal.svelte';
import { getToken } from '$lib/client/auth';


export async function* loadSubtitles(subtitles: Array<Subtitle>) {
  let currentPromise = loadContent(subtitles[0]);
  let nextPromise = currentPromise;
  for (let i = 1; i < subtitles.length; i++) {
    nextPromise = loadContent(subtitles[i]);
    await currentPromise;
    yield subtitles[i - 1];
    currentPromise = nextPromise;
  }
  yield subtitles[subtitles.length - 1];
}

async function loadContent(subtitle: Subtitle) {
  return Promise.all([
    contentAdd(subtitle.videoUrl),
    contentAdd(subtitle.pngUrl)]
  );
}

export async function getSubtitle(id: string): Promise<Subtitle> {
  const response = await fetch(`${SERVER_URL}/v2/subtitle/${id}`);
  return response.json();
}

export async function addSubtitle(subtitle: Subtitle):Promise<Subtitle> {
  const token = await getToken();
  const response = await fetch(`${SERVER_URL}/v2/subtitle`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(subtitle)
  });
  return response.json();
}

export async function getContext(subtitle: Subtitle, previous: number|undefined, next: number|undefined): Promise<SubtitleContext> {
  const subtitles = [];
  let plus = 0, minus = 0;
  if (previous !== undefined && previous > 0) {
    const previousSubtitles: Subtitle[] = await fetch(subtitle.previousSubtitleUrl + '?count=' + previous).then(r => r.json());
    subtitles.push(...previousSubtitles);
    minus = subtitle.startTime - previousSubtitles[0].startTime;
  }
  subtitles.push(subtitle);
  if (next !== undefined && next > 0) {
    const nextSubtitles: Subtitle[] = await fetch(subtitle.nextSubtitleUrl + '?count=' + next).then(r => r.json());
    subtitles.push(...nextSubtitles);
    plus = nextSubtitles[nextSubtitles.length - 1].endTime - subtitle.endTime;
  }
  return {
    previousCount: previous||0,
    nextCount: next||0,
    plusMs: plus,
    minusMs: minus,
    subtitles
  };
}

export type Reading = {
  reading: string;
  hiragana: string;
  romaji: string;
}
const BLANK_READING: Reading = { reading: '', hiragana: '', romaji: '' };
const readingCache = new Map<string, Reading>();

export async function getReading(subtitle: Subtitle | undefined, kanji: string | undefined = undefined): Promise<Reading> {
  if (subtitle)
    kanji = subtitle.kanji;
  if (!kanji)
    return BLANK_READING;
  if (readingCache.has(kanji)) {
    return readingCache.get(kanji)!;
  }
  if (kanji) {
    const s = encodeURI(kanji).replaceAll('.', '%2E').replaceAll('/', '%2F').replaceAll('?', '%3F').replaceAll('!', '').replaceAll('#', '%23').replaceAll('&', '%26');
    const response = await fetch(`${SERVER_URL}/v1/japanese/${s}/reading`, { credentials: 'include' });
    const reading = await response.json();
    readingCache.set(kanji, reading);
    return reading;
  }

  return BLANK_READING;
}

export function openShareModal(modalStore: ModalStore, props: {
  subtitle: Subtitle,
  subtitles?: Subtitle[],
  nextCount?: number,
  previousCount?: number
}) {
  const component: ModalComponent = {
    ref: ShareModal,
    props
  };
  const settings: ModalSettings = {
    type: 'component',
    component
  };
  modalStore.trigger(settings);
}
