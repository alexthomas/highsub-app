import { PUBLIC_SERVER_URL } from '$env/static/public';
import type { DrawerSettings } from '@skeletonlabs/skeleton';

export const SERVER_URL = PUBLIC_SERVER_URL;
export const SIDE_DRAWER_CONFIG: DrawerSettings = { position: 'right', width: 'w-[800px]',bgDrawer:'transparent' };
export const BOTTOM_DRAWER_CONFIG: DrawerSettings = { position: 'bottom', width: 'w-full',bgDrawer:'transparent',duration:1,height:'h-full',opacityTransition:false };
export const ANIME_QA_HOST = 'https://anime-qa.alexk8s.com';
