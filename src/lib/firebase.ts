export const COLLECTIONS = {
  SHOWS: 'shows',
  USERS: 'users',
  INDEXING_STATUS: 'indexing-status',
  PUBLIC_EPISODES: 'publicEpisodes',
  EPISODES: 'episodes',
  CATALOGS: 'catalogs',
  ARTICLES: 'articles',
  COLLECTIONS: 'collections',
  COLLECTION_CLIPS: 'clips',
  SAVED: 'savedSubtitles',
  LIKED: 'likedSubtitles',
  EDITOR_PICKS: 'editorPicks',
  COMMENTS: 'comments',
  GUESSES: 'guesses',
  MEMORY_RESULTS: 'memoryResults',
  PUBLIC_SAVED: 'publicSaved',
};

export function encodeId(id: string): string {
  return id.replaceAll('.', '%2E')
    .replaceAll('$', '%24')
    .replaceAll('[', '%5B')
    .replaceAll(']', '%5D')
    .replaceAll('#', '%23')
    .replaceAll('/', '%2F');
}

export function getCatalogKey(type: 'shows' | 'adminShows' | 'episodes', additionalKey?: string): string {
  if (type === 'episodes') {
    if (!additionalKey) {
      throw new Error('additionalKey is required for episodes catalog');
    }
    return `${COLLECTIONS.CATALOGS}/${additionalKey}-${type}`;
  }
  return `${COLLECTIONS.CATALOGS}/${type}`;
}
