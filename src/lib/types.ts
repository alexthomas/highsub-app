export type Subtitle = {
  id: string,
  show: string,
  season: string,
  episode: string,
  subtitleNumber: number,
  createdUtc: number,
  startTime: number,
  endTime: number,
  japaneseStartTime: number,
  japaneseEndTime: number,
  japaneseSubtitleNumber: number,
  kanji: string,
  romaji: string,
  matchRate: string,
  style: string,
  text: string,
  tags: string[],
  parentTags: string[],
  videoUrl: string,
  pngUrl: string,
  gifUrl: string, //todo delete this field
  shellUrl: string,
  nextSubtitleUrl: string,
  previousSubtitleUrl: string,
  moreFromEpisodeUrl: string,
  name: string,
  domain?: string,
  context: string[],
  contextUrlParams?: string, //todo delete this field,
  shouldIndex: boolean,
  type: string
};

export type SubtitleContext = {
  previousCount: number,
  nextCount: number,
  plusMs: number,
  minusMs: number,
  subtitles: Subtitle[],
}

export type ContextualSubtitle = { subtitle: Subtitle, context: SubtitleContext };

export type SavedSubtitle = {
  id: string
  subtitle: Subtitle,
  context?: SubtitleContext,
  savedAt: string,
  overlayElements?: OverlayElement[],
  imageKey?: string,
  publicKey?: string,
  title?: string,
}

export type OverlayElement = { id: string, type: string,x:number,y:number } & { type: 'text', text: string,fill:string,stroke:string,fontSize:number }

export type Show = {
  kitsuAnimeId?: string,
  year?: number,
  plexKey: string,
  priority: number,
  posterUrl: string,
  showAsPopular: boolean,
  tags: string[],
  show: string,
  seasons: string[],
  hasIndexedEpisodes: boolean,
  episodeIds: string[],
  anime: boolean,
  summary: string,
  indexedEpisodeCount: number,
}

export type SpoilerConfig = {
  show: string,
  seasons: { [key: string]: string[] }
}

export type EpisodeId = { show: string, season: string, episode: string };

export type Episode = EpisodeId & {
  id: string,
  title: string,
  summary?: string,
  studio?: string,
  endingStart?: number,
  endingEnd?: number,
  openingStart?: number,
  openingEnd?: number
  originallyAvailableAt?: string,
  previewStart?: number,
  previewEnd?: number,
  prologueStart?: number,
  prologueEnd?: number,
  subtitleCount?: number,
  height?: number,
  width?: number,
  tags?: string[],
  parentTags?: string[],
  sortKey?: string,
}

export type Reference = { show: string, season: string, episode: string, summary: string, event: string };

export type QuizQuestion = {
  question: string,
  choices: string[],
  answer: string,
  id: string
}

export type Quiz = {
  questions: QuizQuestion[],
  id: string,
  title: string,
  show: string,
  season: string,
  episode: string
  episodeId: string
}

export type QueryAdvice = {
  message: string,
  suggestions: string[],
}

export type HeadData = Partial<{
  title: string,
  description: string,
  url: string,
  videoUrl: string,
  imageUrl: string,
}>
