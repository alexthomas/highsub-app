import { UnleashClient } from 'unleash-proxy-client';
import { writable } from "svelte/store";
import { browser } from "$app/environment";

export const unleash = new UnleashClient({
  url: 'https://unleash-highsub.alexk8s.com/proxy',
  clientKey: 'QrhMrR4MRG6Knj365otCUULvm',
  appName: 'production'
});

// Start the background polling
if(browser)
  unleash.start().then();
export const client = writable(unleash)
unleash.on('update',()=>{
  client.update(()=>unleash);
})