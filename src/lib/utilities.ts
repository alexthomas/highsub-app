import type { EpisodeId, Subtitle } from '$lib/types';

export function debounce(func: (...args) => void, delay: number, immediate = true): (...args) => void {
	let timer: unknown | undefined;
	let args: unknown;
	let tailed = false;

	return function() {
		const context = this;
		args = arguments;
		if (timer) {
			if (immediate && !tailed) {
				tailed = true;
				clearTimeout(timer);
				timer = setTimeout(() => {
					func.apply(context, args);
					timer = undefined;
				},delay);
			}

		} else {
			if (immediate) {
				func.apply(context, args);
				timer = setTimeout(() => timer = undefined, delay);
				tailed = false;
			} else {
				timer = setTimeout(() => {
					func.apply(context, args);
					timer = undefined;
				}, delay);
			}
		}
	};
}

export function applyHeight(url: URL | string, height: number): URL {
	const newUrl = new URL(url.toString());
	newUrl.searchParams.set('height', height.toString());
	return newUrl;
}

export function applyExtension(url: URL | string,extension:string):URL{
	const newUrl = new URL(url.toString());
	newUrl.pathname = newUrl.pathname.substring(0,newUrl.pathname.lastIndexOf('.'))+extension;
	return newUrl;
}

export function applyCountHit(url: URL | string,countHit:boolean): URL {
	const newUrl = new URL(url.toString());
	newUrl.searchParams.set('countHit', countHit?'true':'false');
	return newUrl;
}

export function parseId(id: string): { show: string, season: string, episode: string, subtitle: string } | undefined {
	const idRegex = /(?<show>.+?)-(?<season>[^-]*)-(?<episode>\d+)-(?<subtitle>\d+)/;
  return idRegex.exec(id)?.groups;
}

export function delay(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}


export function getRandomString() {
	return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}


export function getEpisodeDisplay(subtitle:Pick<Subtitle,'episode'|'season'>):string{
	let season = subtitle.season;
	if(season.toLowerCase().startsWith("season"))
		season = season.substring(6).trim();
	const episodePadding = '0'.repeat(Math.max(2-subtitle.episode.length,0));
	if(isNaN(Number(season)))
		return `${season} E${episodePadding+subtitle.episode}`;
	return `S${'0'.repeat(2-season.length)+season} E${episodePadding+subtitle.episode}`;
}

export function downloadJson(object, filename:string){
	const data = JSON.stringify(object, null, 2);
	const blob = new Blob([data], { type: 'application/json' });
	const url = window.URL.createObjectURL(blob);
	const a = document.createElement('a');
	a.href = url;
	a.download=filename;
	document.body.appendChild(a);
	a.click();
	a.remove();
	window.URL.revokeObjectURL(url);
}

export function getSortKey(episode:EpisodeId):string {
  const show = episode.show;
  const season = getSeasonNumber(episode.season);
  const episodeNumber = episode.episode.padStart(5, "0");
  return `${show}-${season}-${episodeNumber}`;
}

function getSeasonNumber(season:string):string {
  const seasonMatch = season.match(/Season (\d+)/);
  if (seasonMatch) {
    return seasonMatch[1].padStart(3, "0");
  }
  return season;
}
