import { redirect } from '@sveltejs/kit';
import type { LayoutServerLoad } from './$types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';

export const load: LayoutServerLoad = async ({ cookies, fetch }) => {
  const token = cookies.get('token') || '';
  const response = await fetch(`${DATA_FUNCTION_URL}/auth/check-admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ idToken: token })
  });
  const { admin } = await response.json();
  console.log(admin)
  if (!admin) {
    return redirect(307, '/login');
  }
};