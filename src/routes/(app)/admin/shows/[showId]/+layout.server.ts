import type { LayoutServerLoad } from './$types';
import { DEFAULT_HEADERS } from '$lib/ServerUtilities';
import { COLLECTIONS, encodeId } from '$lib/firebase';
import type {  Show } from '$lib/types';
import { SERVER_URL } from '$lib/constants';
import type { AdminEpisode, PlexEpisode } from '../../types';
import { getSortKey } from '$lib/utilities';


export const load: LayoutServerLoad = async ({ params, cookies }) => {
  const showId = params.showId;
  const [show, adminEpisodes] = await Promise.all([getShow(showId), getEpisodes(showId)]);
  if (!show) {
    throw {
      status: 404,
      error: new Error('Show not found')
    };
  }
  const plexEpisodes = await getPlexEpisodes(show.plexKey, cookies.get('token') as string);
  return { show, adminEpisodes, plexEpisodes };
};

async function getShow(show: string): Promise<Show | null> {
  const document = await db.collection(COLLECTIONS.SHOWS).doc(encodeId(show)).get();
  if (document.exists) {
    return document.data() as Show;
  }
  return null;
}

async function getEpisodes(show: string): Promise<AdminEpisode[]> {
  const documents = await db.collection(COLLECTIONS.SHOWS).doc(encodeId(show))
    .collection(COLLECTIONS.EPISODES).get();
  return documents.docs.map(doc => ({...doc.data(),id:doc.id}) as AdminEpisode)
    .sort((a,b)=>getSortKey(a).localeCompare(getSortKey(b)));
}

async function getPlexEpisodes(plexKey: string, token: string): Promise<PlexEpisode[]> {
  const response = await fetch(`${SERVER_URL}/v1/plex/shows/${plexKey}/episodes`, {
    headers: {
      ...DEFAULT_HEADERS,
      Authorization: `Bearer ${token}`
    }
  });
  return await response.json();
}