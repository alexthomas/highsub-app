import type { LayoutServerLoad } from './$types';
import { SERVER_URL } from '$lib/constants';
import type { Subtitle } from '$lib/types';
import type { AdminEpisode, PlexEpisode } from '../../../../types';
import { error } from '@sveltejs/kit';
import { encodeId } from '$lib/firebase';
import { DEFAULT_HEADERS } from '$lib/ServerUtilities';

export const load: LayoutServerLoad = async ({ params, parent }) => {
  const episodeId = encodeId(params.episodeId)
  console.log(episodeId)
  const episodePromise = parent().then(({adminEpisodes,plexEpisodes})=>getEpisodeData(adminEpisodes, plexEpisodes, episodeId));
  const subtitlesPromise = getSubtitles(params.episodeId);
  const [episodeData, subtitles] = await Promise.all([episodePromise, subtitlesPromise]);
  if(!episodeData.adminEpisode) return error(404, 'Episode not found');

  return {
    ...episodeData,
    subtitles
  }
};

function getEpisodeData(adminEpisodes: AdminEpisode[], plexEpisodes: PlexEpisode[], episodeId: string): {
  adminEpisode?: AdminEpisode,
  plexEpisode?: PlexEpisode
} {
  if (!adminEpisodes) return {};
  const adminEpisode = adminEpisodes.find(episode => episode.id === episodeId);
  if (!adminEpisode) return {};
  if (plexEpisodes) {
    const plexEpisode = plexEpisodes.find(episode => episode.season === adminEpisode.season && episode.episode === adminEpisode.episode);
    if (plexEpisode) {
      return {
        adminEpisode, plexEpisode
      };
    }
  }
  return { adminEpisode };
}

async function getSubtitles(episodeId: string) {
  const response = await fetch(`${SERVER_URL}/v1/subtitle/episode/${encodeURIComponent(episodeId)}`,{headers:DEFAULT_HEADERS});
  return (await response.json()) as Subtitle[];
}