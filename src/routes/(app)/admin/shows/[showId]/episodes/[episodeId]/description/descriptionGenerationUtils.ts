import type { Episode, Subtitle } from '$lib/types';
import type { EpisodeContext,  } from './types';
import { datadogLogs } from '@datadog/browser-logs';
import { ANIME_QA_HOST } from '$lib/constants';

const BLACKLIST_STYLE_WORDS = ['title', 'stuff', 'next_time', 'preview', 'disclaimer', 'lyrics', 'next ep', 'logo', 'next time', 'made-up language'];
const WHITELIST_STYLE_WORDS = ['default', 'subtitle', 'dialog', 'main'];

export function getStyleClassifications(subtitles: Subtitle[]) {
	const uniqueStyles: Set<string> = subtitles.map(s => s.style).reduce((acc, val) => acc.add(val), new Set<string>());
	return [...uniqueStyles].map(style => {
		const lowerStyle = style.toLowerCase();
		return {
			style,
			shouldIndex: WHITELIST_STYLE_WORDS.some(word => lowerStyle.includes(word)) || !BLACKLIST_STYLE_WORDS.some(word => lowerStyle.includes(word))
		};
	});
}


export async function getContext(episode: Episode): Promise<EpisodeContext[]> {
	const episodeId = encodeURIComponent(`${episode.show}|${episode.season}|${episode.episode}`);
	const response = await fetch(`${ANIME_QA_HOST}/v1/episode-context/episode/${episodeId}`);
	if (!response.ok) {
		datadogLogs.logger.error('Failed to get summaries', { episode });
		throw new Error('Failed to get summaries');
	}
	return response.json();
}

export async function saveContexts(episode:Episode,contexts:EpisodeContext[]){
	const episodeId = encodeURIComponent(`${episode.show}|${episode.season}|${episode.episode}`);
	const response = await fetch(`${ANIME_QA_HOST}/v1/episode-context/episode/${episodeId}`,{
		method:'POST',
		headers:{
			'Content-Type':'application/json'
		},
		body:JSON.stringify(contexts)
	});
	if (!response.ok) {
		datadogLogs.logger.error('Failed to save episode contexts', { episode });
		throw new Error('Failed to save episode contexts');
	}
}

export async function generateEpisodeContext(episode: Episode) {
	const { show, season, episode: episodeNumber } = episode;
	const requestBody = [{ show, season, episode: episodeNumber }];
	const response = await fetch(`${ANIME_QA_HOST}/v1/episode-context`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(requestBody)
	});
	if (!response.ok) {
		datadogLogs.logger.error('Failed to generate episode context', { episode });
		throw new Error('Failed to generate episode context');
	}
}

export async function isQueued(episode: Episode): Promise<boolean> {
	type EpisodeReference = { show: string, season: string, episode: string };
	type Queue = { pending: EpisodeReference[], completed: EpisodeReference[] };
	const response = await fetch(`${ANIME_QA_HOST}/v1/episode-context/queue`);
	if (!response.ok) {
		datadogLogs.logger.error('Failed to get queue');
		throw new Error('Failed to get queue');
	}
	const queue: Queue = await response.json();
	return queue.pending.some(e => e.show === episode.show && e.season === episode.season && e.episode === episode.episode);
}


