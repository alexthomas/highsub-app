export type StyleClassification = {
	style: string,
	shouldIndex: boolean
}

export type EpisodeSummary = {
	id: string,
	show: string,
	season: string,
	episode: string,
	title: string,
	generationPrompt: string,
	generatedSummary: string,
	plexSummary?: string,
	promptVersion:string,
}

export type EventSummary = EpisodeSummary & {event:string};

export type EpisodeContext = EpisodeSummary & {
	type: string,
	sequence:number,
	reviewed:boolean,
}