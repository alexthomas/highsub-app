import type { Episode, EpisodeId } from '$lib/types';
import type { EpisodeContext } from './shows/[showId]/episodes/[episodeId]/description/types';

export type AdminEpisode = Episode & {
  englishSubtitles: string,
  japaneseSubtitles: string,
  scaled150pVideo: string,
  scaled150pVideoBytes: number,
  scaled480pVideo: string,
  scaled480pVideoBytes: number,
  sourceVideo: string,
  sourceVideoBytes: number
};

export type PlexEpisode = EpisodeId & {
  title: string,
  summary: string,
  studio: string,
  videoFile: string
};

export type IndexingStatus = {
  show: string,
  season: string,
  episode: string,
  message: string,
  totalTaskQuantity: number,
  taskQuantity: number,
  status: 'STARTING' | 'EXTRACTING_SUBTITLES' | 'EXTRACTING_DURATION' | 'HASHING_FILE' | 'COMPLETE' | 'ERROR',
  timestamp: number
}

export type IndexingEpisode = EpisodeId & {
  adminEpisode?: AdminEpisode,
  plexEpisode?: PlexEpisode,
  indexingStatus?: IndexingStatus,
  indexed: boolean,
  scaled: boolean,
  subtitleCount: number,
  title: string,
  episodeContext?:EpisodeContext
}


export type Season = {
  season: string,
  episodes: IndexingEpisode[],
};
