import type { ArticleListing } from './types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import { error } from '@sveltejs/kit';

export async function load({fetch}) {
  const response = await fetch(`${DATA_FUNCTION_URL}/article`);
  if(response.ok){
    const {articles} = await response.json() as {articles: ArticleListing[]};
    return {articles};
  }

  return error(500, 'Failed to load data');
}