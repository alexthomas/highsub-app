import type { Article, ArticleListing } from '../types';


const GITLAB_URL = 'https://gitlab.com/alexthomas/anime-articles/-/raw/main/';

export async function load({ params, fetch }) {
  const article = await getArticle(params.id, fetch);
  const { id, title, tags, content, publishedUtc, readers } = article;
  const otherArticles: ArticleListing[] = []; //todo fetch other articles
  return { article: { id, title, tags, content, publishedUtc, readers }, otherArticles };
}

async function getArticle(id: string, fetch: (input: RequestInfo, init?: RequestInit) => Promise<Response>): Promise<Article> {
  const response = await fetch(`${GITLAB_URL}${id}`);
  return await response.json();
}




