import type { Message } from 'ai';
import type { Reference } from '$lib/types';

export type Article = {
	id: string,
	title: string,
	tags: string[],
	content: Content[],
	publishedUtc: number,
	prompt?: Prompt,
	messages?: Message[],
	readers?: Readings,
	references?: Reference[]
}

export type Content =
	{ type: 'header', text: string, readers?: Readings } |
	{ type: 'paragraph', text: string, readers?: Readings } |
	{ type: 'image', imageDescription: string, subtitleId: string, previous?: number, next?: number } |
	{ type: 'imagePlaceholder', imageDescription: string }

export type Prompt = {
	show: string,
	thesis: string,
	points: string[]
}

export type ArticleListing = {
	title: string,
	tags: string[],
	repoId: string,
	publishedUtc: number
}

export type Readers = 'orochimaru';
export type Readings = { [key in Readers]: string }