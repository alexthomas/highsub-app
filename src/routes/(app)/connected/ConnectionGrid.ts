export type Square = {
  x: number,
  y: number,
  isFilled: boolean,
  color: string,
  connectedTo: Square[],
  depth: number,
  updateSequence?: number
};

export type Line = {
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  hash: string,
  color: string,
  updateSequence?: number
};

export type Handler = (squares: Square[], lines: Line[], done: boolean) => void;

export class ConnectionGrid {
  private grid: Square[][];
  private squares: Square[] = [];
  private lines: Line[] = [];
  private lineHashes: Set<string> = new Set();
  private currentSquares: Square[];
  private handlers: Handler[] = [];
  private updateSequence = 0;

  constructor(readonly width: number, readonly height: number, readonly colors: string[]) {
    this.grid = [];
    for (let i = 0; i < height; i++) {
      this.grid[i] = [];
    }
    this.currentSquares = colors.map(this.randomSquare.bind(this));
    this.currentSquares.forEach(s => s.isFilled = true);
  }

  private randomSquare(color: string): Square {
    for (let i = 0; i < 100; i++) {
      try {
        const x = Math.floor(Math.random() * this.width);
        const y = Math.floor(Math.random() * this.height);
        const square = {
          x, y, isFilled: false, connectedTo: [], color, depth: 0, updateSequence: this.updateSequence++
        };
        this.addSquare(square);
        return square;
      } catch (e) {
        console.warn(e);
      }
    }
    throw new Error("Could not find a random square");
  }

  private getSurroundingSquares({ x, y, color }: Square): Square[] {
    const surroundingSquares: Square[] = [];
    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        if (i === 0 && j === 0) continue;
        const newX = x + i;
        const newY = y + j;
        if (newX < 0 || newX >= this.width || newY < 0 || newY >= this.height) continue;
        if (!this.grid[newY][newX]) {
          const square: Square = {
            x: newX,
            y: newY,
            isFilled: false,
            color,
            connectedTo: [],
            depth: 0,
            updateSequence: this.updateSequence++
          };
          this.addSquare(square);
          surroundingSquares.push(square);
        } else if (this.grid[newY][newX].color === color) {
          surroundingSquares.push(this.grid[newY][newX]);
        }
      }
    }
    return surroundingSquares;
  }

  private addSquare(square: Square) {
    if (this.grid[square.y][square.x])
      throw new Error("Square already exists");
    this.squares.push(square);
    this.grid[square.y][square.x] = square;
  }

  private getLine(s1: Square, s2: Square): Line {
    return {
      x1: s1.x,
      y1: s1.y,
      x2: s2.x,
      y2: s2.y,
      hash: `${s1.x},${s1.y},${s2.x},${s2.y}`,
      color: s1.color,
      updateSequence: this.updateSequence++
    };
  }

  private findNextToHollow(square: Square): Square | undefined {
    const hollow = this.squares.findLast(s => !s.isFilled && s.color === square.color);
    if (!hollow)
      return undefined;
    const direction = {
      x: hollow.x - square.x,
      y: hollow.y - square.y
    };
    const magnitude = this.getDistance(square, hollow);
    direction.x = Math.round(direction.x / magnitude);
    direction.y = Math.round(direction.y / magnitude);
    const target = { x: square.x + direction.x, y: square.y + direction.y };
    if (this.grid[target.y][target.x]) {
      if (this.grid[target.y][target.x].color === square.color)
        return this.grid[target.y][target.x];
      return this.getSurroundingSquares(square).filter(s => s.depth < 1.5)
        .map(s => ({ s, distance: this.getDistance(s, hollow) }))
        .sort((a, b) => a.distance - b.distance)
        .map(x=>x.s)[0];
    }
    const newSquare: Square = {
      ...target,
      connectedTo: [],
      isFilled: false,
      color: square.color,
      depth: 0,
      updateSequence: this.updateSequence++
    };
    this.addSquare(newSquare);
    return newSquare;

  }

  private getDistance(s1: Square, s2: Square): number {
    const deltaX = s1.x - s2.x;
    const deltaY = s1.y - s2.y;
    return Math.sqrt(deltaX ** 2 + deltaY ** 2);
  }

  public addHandler(handler: Handler) {
    this.handlers.push(handler);
  }

  public removeHandler(handler: Handler) {
    this.handlers = this.handlers.filter(h => h !== handler);
  }

  public tick() {
    this.currentSquares = this.currentSquares.map(this.tickSquare.bind(this))
      .filter(s => s !== undefined) as Square[];
    this.squares.filter(s => s.depth > 1)
      .forEach(s => {
        s.depth = Math.max(1, s.depth - .1);
        s.updateSequence = this.updateSequence++;
      });
    const squares = [...this.squares];
    const lines = [...this.lines];
    this.handlers.forEach(h => h(squares, lines, this.currentSquares.length === 0));
  }

  private tickSquare(currentSquare: Square): Square | undefined {
    const surroundingSquares = this.getSurroundingSquares(currentSquare);
    const nextSquares = surroundingSquares.filter(s => !s.isFilled && s.color === currentSquare.color);
    let nextSquare: Square;
    if (nextSquares.length === 0) {
      const nextToHollow = this.findNextToHollow(currentSquare);
      if (nextToHollow) {
        nextSquare = nextToHollow;
        currentSquare.connectedTo.push(nextSquare);
        nextSquare.updateSequence = this.updateSequence++;
        currentSquare.updateSequence = this.updateSequence++;
        if (nextSquare.isFilled)
          nextSquare.depth = nextSquare.depth + 1;
      } else {
        nextSquare = surroundingSquares[Math.floor(Math.random() * surroundingSquares.length)];
        if (!currentSquare.connectedTo.includes(nextSquare) && !nextSquare.connectedTo.includes(currentSquare)) {
          nextSquare.depth = nextSquare.depth + 1;
          nextSquare.updateSequence = this.updateSequence++;
          currentSquare.updateSequence = this.updateSequence++;
          currentSquare.connectedTo.push(nextSquare);
        } else {
          return undefined;
        }
      }
    } else {
      nextSquare = nextSquares[Math.floor(Math.random() * nextSquares.length)];
      currentSquare.connectedTo.push(nextSquare);
      nextSquare.connectedTo.push(currentSquare);
      nextSquare.isFilled = true;
      nextSquare.updateSequence = this.updateSequence++;
      currentSquare.updateSequence = this.updateSequence++;
    }
    const line = this.getLine(currentSquare, nextSquare);
    if (!this.lineHashes.has(line.hash))
      this.addLine(line);
    this.getSurroundingSquares(nextSquare);//load new hollow squares around next square
    return nextSquare;
  }

  private addLine(line: Line) {
    if (this.lineHashes.has(line.hash))
      throw new Error("Line already exists");
    this.lineHashes.add(line.hash);
    this.lines.push(line);
  }
}