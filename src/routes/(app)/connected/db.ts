import Dexie, { type IndexableType, type Table } from "dexie";
import type { ConnectionGrid, Handler, Line, Square } from "./ConnectionGrid";

type ConnectedGridRecord = {
  id?: number,
  lastUpdateSequence: number,
  colors: string[],
  width?: number,
  height?: number,
  created?: Date,
  updated?: Date,
  isDone?: boolean,
}

type ConnectedGridSquareRecord = {
  id?: IndexableType,
  gridId: IndexableType,
  color: string,
  x: number,
  y: number,
  updateSequence?: number,
  connectedTo?: { x: number, y: number }[],
}

type ConnectedGridLineRecord = {
  id?: IndexableType,
  gridId: IndexableType,
  color: string,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  updateSequence?: number,
}

class ConnectedGridDatabase extends Dexie {
  connectedGrids!: Table<ConnectedGridRecord>;
  connectedGridSquares!: Table<ConnectedGridSquareRecord>;
  connectedGridLines!: Table<ConnectedGridLineRecord>;

  constructor() {
    super("highsub-connected");
    this.version(3).stores({
      connectedGrids: "++id,lastUpdateSequence",
      connectedGridSquares: "++id,gridId,color,[gridId+x+y]",
      connectedGridLines: "++id,gridId,color,[gridId+x1+y1+x2+y2]"
    });
    this.connectedGrids.where("lastUpdateSequence").below(0).delete().then((n) => console.log(`Deleted ${n} empty grids`));
  }
}

let db: ConnectedGridDatabase;
const MB_BYTES = 1024 ** 2;
const idCache = new Map<string, IndexableType>();

export async function writeGrid(grid: ConnectionGrid, colors: string[]) {
  if (!db) {
    db = new ConnectedGridDatabase();
  }
  idCache.clear();
  const storageEstimate = await navigator.storage.estimate();
  if (storageEstimate) {
    let quota = (storageEstimate.quota || 0) / MB_BYTES;
    let quotaSuffix = "MB";
    if (quota > 1024) {
      quota /= 1024;
      quotaSuffix = "GB";
    }
    console.log(`Storage estimate: ${((storageEstimate.usage || 0) / MB_BYTES).toFixed(1)}MB used of ${quota.toFixed(1)}${quotaSuffix}`);
  }
  const gridId: IndexableType = await db.connectedGrids.add({ colors, lastUpdateSequence: -1, width: grid.width, height: grid.height, created: new Date(), updated: new Date(), isDone: false });
  const handler: Handler = (s, l, d) => writeUpdates(grid, gridId, s, l, d);
  grid.addHandler(handler);
}

async function writeUpdates(grid: ConnectionGrid, gridId: IndexableType, squares: Square[], lines: Line[], isDone: boolean) {
  const gridRecord = await db.connectedGrids.get(gridId);
  if (gridRecord === undefined) {
    console.error("Grid record not found");
    return;
  }
  const squaresToWrite: ConnectedGridSquareRecord[] = squares
    .filter(s => s.updateSequence && s.updateSequence > gridRecord.lastUpdateSequence)
    .map(s => ({ gridId, ...s }))
    .map((s: ConnectedGridSquareRecord) => {
      s.connectedTo = s.connectedTo?.map(c => ({ x: c.x, y: c.y }));
      return s;
    })
  ;
  const linesToWrite: ConnectedGridLineRecord[] = lines
    .filter(l => l.updateSequence && l.updateSequence > gridRecord.lastUpdateSequence)
    .map(l => ({ gridId, ...l }));
  for (const s of squaresToWrite) {
    const cacheKey = `${s.gridId}-${s.x}-${s.y}`;
    if (idCache.has(cacheKey)) {
     s.id  = idCache.get(cacheKey);
    }
  }
 const ids = await db.connectedGridSquares.bulkPut(squaresToWrite, { allKeys: true });
  for (let i = 0; i < ids.length; i++) {
    const cacheKey = `${squaresToWrite[i].gridId}-${squaresToWrite[i].x}-${squaresToWrite[i].y}`;
    idCache.set(cacheKey, ids[i]);
  }
  for (const l of linesToWrite) {
    const cacheKey = `${l.gridId}-${l.x1}-${l.y1}-${l.x2}-${l.y2}`;
    if (idCache.has(cacheKey)) {
      l.id  = idCache.get(cacheKey);
    }
  }
  const lineIds = await db.connectedGridLines.bulkPut(linesToWrite, { allKeys: true });
  for (let i = 0; i < lineIds.length; i++) {
    const cacheKey = `${linesToWrite[i].gridId}-${linesToWrite[i].x1}-${linesToWrite[i].y1}-${linesToWrite[i].x2}-${linesToWrite[i].y2}`;
    idCache.set(cacheKey, lineIds[i]);
  }
  const lastUpdateSequence = Math.max(...squaresToWrite.map(s => s.updateSequence || 0), ...linesToWrite.map(l => l.updateSequence || 0));
  await db.connectedGrids.update(gridId, { lastUpdateSequence, isDone, updated: new Date() });
}