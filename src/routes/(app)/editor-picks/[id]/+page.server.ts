import type { PageServerLoad } from './$types';
import { error } from '@sveltejs/kit';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';

export const load: PageServerLoad = async ({ params }) => {
  const response = await fetch(`${DATA_FUNCTION_URL}/editor-pick/${params.id}`);
  const data = await response.json();
  if (!data) {
    return error(404, 'Document not found');
  }
  if (data.published) {
    return { pick: data };
  }
  return error(404, 'Document not found');
};