import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import type { EditorPick } from '../../types';

export const load: PageServerLoad = async ({ cookies, params, fetch }) => {
  const token = cookies.get('token') || '';
  const response = await fetch(`${DATA_FUNCTION_URL}/auth/check-admin`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ idToken: token })
  });
  const { isAdmin } = await response.json();
  if (!isAdmin) {
    return redirect(307, '/login');
  }
  const data: Partial<EditorPick> = await fetch(`${DATA_FUNCTION_URL}/editor-pick/${params.id}`).then(r => r.json());
  if (!data) {
    return {
      status: 404,
      error: 'Document not found'
    };
  }
  return { pick: data };
};