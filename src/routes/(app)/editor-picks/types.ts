import type { ContextualSubtitle } from '$lib/types';

export type TextSection = {
  id: string,
  text: string,
  type: 'text'
}

export type SubtitleSection = {
  id: string,
  type: 'subtitle'
  subtitle: ContextualSubtitle
}
export type ColumnSection = TextSection | SubtitleSection;
export type TwoColumnSection = {
  id: string,
  type: 'two-column',
  left: ColumnSection,
  right: ColumnSection
}

export type Section = ColumnSection | TwoColumnSection;

export type EditorPick = {
  authoredBy: string,
  authoredDate: string,
  publishedDate: string,
  published: boolean,
  title: string,
  tags: string[],
  shows: string[],
  episodes: string[],
  sections: Section[],
}

