import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import type { Show } from '$lib/types';
import type { LayoutServerLoad } from './$types';

let anime: Show[] | null = null;

export const load: LayoutServerLoad = async ({ fetch }) => {
  if (anime === null) {
    const response = await fetch(`${DATA_FUNCTION_URL}/show/anime`);
    if (response.ok) {
      anime = await response.json() as Show[];
    }
  }
  return { anime };
};