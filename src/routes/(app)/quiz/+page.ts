import type { PageLoad } from './$types';
import quizzes from './quizzes.json';
import { type Quiz } from '$lib/types'


export const load: PageLoad = () => {
    const shows = getShows();
    const quizPerShow = getQuizPerShow(shows);
    return {shows,quizPerShow};
}


function getShows():string[]{
    return quizzes.map(q => q.show).reduce((acc, show) => {
		if (!acc.includes(show)) {
			acc.push(show);
		}
		return acc;
	}, [] as string[]);
}

function getQuizPerShow(shows:string[]):{[key:string]:Quiz} {
    return shows.reduce((acc,show)=>{acc[show]=getRandomQuiz(show);return acc;},{} as {[key:string]:Quiz});
}

function getRandomQuiz(show:string){
    const quizzesForShow = quizzes.filter(q=>q.show===show);
    return quizzesForShow[Math.floor(Math.random()*quizzesForShow.length)];
}