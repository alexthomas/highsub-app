import type { PageLoad } from './$types';
import { ANIME_QA_HOST } from '$lib/constants';
import quizzes from '../quizzes.json';
import { DEFAULT_HEADERS } from '$lib/ServerUtilities';



export const load: PageLoad = async ({fetch,params}) => {
    const response = await fetch(`${ANIME_QA_HOST}/v1/quiz/${encodeURIComponent(params.id)}`,{headers:DEFAULT_HEADERS});
    const quiz = await response.json();
    const nextQuiz = getRandomQuiz(quiz.show,quiz.id);
    return {quiz,nextQuiz};
}

function getRandomQuiz(show:string,ignoreId:string){ //todo: remove duplicate code
    const quizzesForShow = quizzes.filter(q=>q.show===show && q.episodeId!==ignoreId);
    return quizzesForShow[Math.floor(Math.random()*quizzesForShow.length)];
}