import type { Episode, SavedSubtitle } from '$lib/types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import { error } from '@sveltejs/kit';

export type PublicSavedData = {
  subtitle: SavedSubtitle;
  episode: Episode;
  imageUrl: string;
};

export async function load({ params, fetch }) {
  const id = params.id;
  const response = await fetch(`${DATA_FUNCTION_URL}/public-saved/${id}`);
  if(response.ok){
    return await response.json() as PublicSavedData;
  }
  return error(500, 'Failed to load data');

}

