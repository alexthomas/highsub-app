import {
  ContextSubtitleDataExtractor,
  type MediaDataExtractor,
  SubtitleDataExtractor
} from '$lib/client/SubtitleCard/types';
import type { ContextualSubtitle, SavedSubtitle } from '$lib/types';
import { applyHeight } from '$lib/utilities';
import type { ToastStore } from '@skeletonlabs/skeleton';

export const SavedSubtitleDataExtractor: MediaDataExtractor<SavedSubtitle> = {
  getVideoUrl(data: SavedSubtitle, height: number | null, countHit: boolean) {
    return data.context ?
      ContextSubtitleDataExtractor.getVideoUrl(data as ContextualSubtitle, height, countHit)
      : SubtitleDataExtractor.getVideoUrl(data.subtitle, height, countHit);
  },
  getImageUrl(data: SavedSubtitle, height: number | null, countHit: boolean): string {
    return data.context ?
      ContextSubtitleDataExtractor.getImageUrl(data as ContextualSubtitle, height, countHit)
      : SubtitleDataExtractor.getImageUrl(data.subtitle, height, countHit);
  },
  getTags(data: SavedSubtitle): string[] {
    return data.context ?
      ContextSubtitleDataExtractor.getTags(data as ContextualSubtitle)
      : SubtitleDataExtractor.getTags(data.subtitle);
  },
  getId(data: SavedSubtitle): string {
    return data.id;
  },
  getSubtitleLink(data: SavedSubtitle): string {
    return window.location.origin + `/saved/${data.id}`;
  },
  getSubtitleKey(data: SavedSubtitle): { show: string; season: string; episode: string; subtitleNumber: number; } {
    return data.context ?
      ContextSubtitleDataExtractor.getSubtitleKey(data as ContextualSubtitle)
      : SubtitleDataExtractor.getSubtitleKey(data.subtitle);
  },
  getTiming(data: SavedSubtitle): { startTime: number; endTime: number; } {
    return data.context ?
      ContextSubtitleDataExtractor.getTiming(data as ContextualSubtitle)
      : SubtitleDataExtractor.getTiming(data.subtitle);
  },
  getText(data: SavedSubtitle): string {
    return data.context ?
      ContextSubtitleDataExtractor.getText(data as ContextualSubtitle)
      : SubtitleDataExtractor.getText(data.subtitle);
  },
  getShow: function(data: SavedSubtitle): string {
    return data.subtitle.show;
  }
};

export function drawOnCanvas(imageUrl: string, svgElement: SVGElement): Promise<string> {
  const canvas = document.createElement('canvas');
  const image = new Image();
  image.src = imageUrl;
  image.crossOrigin = 'anonymous';
  return new Promise((resolve, reject) => {
    image.onload = function() {
      canvas.width = image.width;
      canvas.height = image.height;
      const context = canvas.getContext('2d');
      context?.drawImage(image, 0, 0);
      const svg = new XMLSerializer().serializeToString(svgElement);
      const svgBlob = new Blob([svg], { type: 'image/svg+xml' });
      const url = URL.createObjectURL(svgBlob);
      const svgImage = new Image();
      svgImage.src = url;
      svgImage.onload = function() {
        context?.drawImage(svgImage, 0, 0);
        URL.revokeObjectURL(url);
        resolve(canvas.toDataURL());
      };
      svgImage.onerror = function() {
        reject('Failed to load svg image');
      };
    };
    image.onerror = function() {
      reject('Failed to load image');
    };
  });
}

export async function downloadImage(subtitle: SavedSubtitle, svgElement: SVGElement|undefined, toastStore: ToastStore) {
  const imageUrl = applyHeight(subtitle.subtitle.pngUrl, -1);
  try {
    const a = document.createElement('a');
    if(svgElement){
      a.href = await drawOnCanvas(imageUrl.toString(), svgElement);
    } else {
      a.href = imageUrl.toString();
    }
    a.download = `${subtitle.subtitle.show}-${subtitle.id}.png`;
    a.click();
  } catch (e) {
    toastStore.trigger({ message: e as string, background: 'variant-filled-error' });
  }
}