import { type Readable, writable } from 'svelte/store';
import { getShows } from '$lib/client/content';
import { getDb, type ParsedQuery, type SearchHistoryRecord } from '$lib/client/db';
import type { Show } from '$lib/types';
import { faHistory, faTag, faTvRetro } from '@fortawesome/pro-solid-svg-icons';
import type { SearchCriteria } from './stores';
import { buildQueryParams } from './searchUtilities';
import { SERVER_URL } from '$lib/constants';
import { goto } from '$app/navigation';
import { getCatalogKey } from '$lib/firebase';
import { doc, getDoc, getFirestore } from '@firebase/firestore';
import { datadogLogs } from '@datadog/browser-logs';
import { browser } from '$app/environment'; //to load the app

const SEARCH_KEY = 'search-wf19cmjmeu5oq8s4qo8aec9k';
const ENDPOINT_BASE = 'https://enterprise-search-highsub.alexk8s.com';
const ENGINE_NAME = 'highsub';
const QUERY_SUGGESTION_URL = `${ENDPOINT_BASE}/api/as/v1/engines/${ENGINE_NAME}/query_suggestion`;

export type SuggestionType = 'autocomplete' | 'show' | 'tag' | 'history';
export type SearchType = 'clips' | 'shows' | 'articles' | 'showClips' | 'episode';
export type Suggestion = {
  suggestion: string;
  type: SuggestionType;
  index: number;
  preview?: string;
  icon?: unknown;
}
export type Suggestions = {
  [K in SuggestionType]: Suggestion[];
}

export type Tag = {
  tag: string;
  priority: number;
}

export interface SearchInputFacilitator {
  updateInput: (input: string) => void;
  getSuggestionStore: () => Readable<Suggestions>;
  search: () => void;
}

type QuerySuggestionRequest = {
  query: string,
  types: {
    documents: {
      fields: string[]
    }
  }
}

type QuerySuggestionResponse = {
  meta: { request_ud: string },
  results: {
    documents: { suggestion: string }[]
  }
}

class ClipSearchInputFacilitator implements SearchInputFacilitator {
  private suggestions = writable({} as Suggestions);
  private shows: Show[] = [];
  private tags: Tag[] = [];
  private searchHistory: SearchHistoryRecord[] = [];
  private searchInput = '';
  private searchParseVersion = '';
  private cachedResponses = new Map<string, QuerySuggestionResponse>();
  private processingAutocomplete = new Set<string>();

  constructor(private suggestionCount: number = 10) {
      this.loadRichSuggestions().then();
      this.updateHistory().then();
  }

  private async loadRichSuggestions() {
    try {
      const catalogKey = getCatalogKey('shows');
      const documentSnapshot = await getDoc(doc(getFirestore(), catalogKey));
      this.shows.push(...documentSnapshot.data()!.shows);
    } catch (e) {
      datadogLogs.logger.warn('Failed to load shows', e as object);
    }
    //todo: add tags back into search
    const history = await getDb().searchHistory.orderBy('searchDate').reverse().toArray();
    this.searchHistory.push(...history);
    this.setHistorySuggestions();
  }

  private setHistorySuggestions() {

    const foundHistory = new Set<string>();
    let totalSuggestions = 0;
    const historySuggestions: Suggestion[] = this.searchHistory
      .filter(s => foundHistory.has(s.searchCriteria.q!) ? false : foundHistory.add(s.searchCriteria.q!))
      .slice(0, 10)
      .map(s => ({ suggestion: s.searchCriteria.q!, type: 'history', index: totalSuggestions++, icon: faHistory }));
    this.suggestions.update(s => ({ ...s, history: historySuggestions }));
  }

  updateInput(input: string) {
    const formattedInput = input.trim().toLowerCase();
    const lastWordIndex = formattedInput.lastIndexOf(' ') + 1;
    const lastWord = formattedInput.substring(lastWordIndex).toLowerCase();
    const previousWords = input.substring(0, lastWordIndex).trim();
    let suggestionCursor = 0;
    const history = this.getHistorySuggestions(formattedInput);
    history.forEach(s => s.index = suggestionCursor++);
    const tag = this.getTagSuggestions(lastWord, previousWords);
    tag.forEach(s => s.index = suggestionCursor++);
    const show = this.getShowSuggestions(lastWord, previousWords);
    show.forEach(s => s.index = suggestionCursor++);
    this.suggestions.update(s => ({ ...s, history, tag, show }));
    this.searchInput = input;
    this.loadAutocompleteSuggestions(input).then();
  }

  private async loadAutocompleteSuggestions(input: string) {
    if (input.trim() === '') {
      this.updateAutocompleteSuggestions([]);
      return;
    }
    if (this.processingAutocomplete.has(input))
      return;
    if (this.cachedResponses.has(input)) {
      this.updateAutocompleteSuggestions(this.cachedResponses.get(input)!.results.documents);
    } else {
      try {
        this.processingAutocomplete.add(input);
        const request: QuerySuggestionRequest = {
          query: input,
          types: {
            documents: {
              fields: ['text']
            }
          }
        };
        const response = await fetch(QUERY_SUGGESTION_URL, {
          method: 'POST',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${SEARCH_KEY}`
          },
          body: JSON.stringify(request)
        });
        const json = await response.json() as QuerySuggestionResponse;
        this.cachedResponses.set(input, json);
        if (input === this.searchInput)
          this.updateAutocompleteSuggestions(json.results.documents);
      } finally {
        this.processingAutocomplete.delete(input);
      }
    }
  }

  private getHistorySuggestions(input: string): Suggestion[] {
    const foundHistory = new Set<string>();
    return this.searchHistory.filter(s => s.searchCriteria.q && s.searchCriteria.q.toLowerCase().includes(input))
      .filter(s => foundHistory.has(s.searchCriteria.q!) ? false : foundHistory.add(s.searchCriteria.q!))
      .map(s => ({ suggestion: s.searchCriteria.q!, type: 'history', index: 0, icon: faHistory }));
  }

  private getTagSuggestions(lastWord: string, previousWords: string): Suggestion[] {
    return this.tags.filter(t => t.tag.toLowerCase().startsWith(lastWord))
      .map(t => ({
        suggestion: `${previousWords} #${t.tag}`.trim(),
        type: 'tag',
        index: 0,
        preview: t.tag,
        icon: faTag
      }));
  }

  private getShowSuggestions(lastWord: string, previousWords: string): Suggestion[] {
    return this.shows.filter(s => s.show.toLowerCase().startsWith(lastWord))
      .map(s => ({
        suggestion: `${previousWords} series:"${s.show}"`.trim(),
        type: 'show',
        index: 0,
        preview: s.show,
        icon: faTvRetro
      }));
  }

  getSuggestionStore() {
    return this.suggestions;
  }

  search() {
    const searchCriteria: SearchCriteria = { q: this.searchInput, page: 0 };
    const searchUrl = this.getSearchUrl(searchCriteria);
    if (searchCriteria.q !== undefined && searchCriteria.q.trim() !== '') {
      this.getParsedQuery(searchCriteria).then(parsedQuery => ({
        searchCriteria,
        searchUrl,
        searchDate: new Date(),
        parseVersion: this.searchParseVersion,
        parsedQuery
      }))
        .then(historyRecord => getDb().searchHistory.add(historyRecord))
        .then();
    }

    if (window.location.pathname !== searchUrl) {
      goto(searchUrl);
    }
  }

  protected getSearchUrl(searchCriteria: SearchCriteria): string {
    return `/search?${this.getUrlParams(searchCriteria)}`;
  }

  protected getUrlParams(searchCriteria: SearchCriteria): URLSearchParams {
    const urlSearchParams = new URLSearchParams();
    if (searchCriteria.q !== undefined)
      urlSearchParams.set('q', searchCriteria.q.trim());
    if (searchCriteria.page !== undefined)
      urlSearchParams.set('page', searchCriteria.page.toString());
    if (searchCriteria.show !== undefined)
      urlSearchParams.set('show', searchCriteria.show);
    return urlSearchParams;
  }

  private async getParsedQuery(searchCriteria: SearchCriteria): Promise<ParsedQuery> {
    const urlSearchParams = buildQueryParams(searchCriteria);
    const url = new URL(`${SERVER_URL}/v2/search/parse?${urlSearchParams.toString()}`);
    const response = await fetch(url.toString(), { credentials: 'include' });
    return await response.json();
  }

  private async updateHistory() {
    this.searchParseVersion = await (await fetch(`${SERVER_URL}/v2/search/version`, { credentials: 'include' })).text();
    const searchHistory = getDb().searchHistory;
    const toUpdate: SearchHistoryRecord[] = await searchHistory.where('parseVersion').notEqual(this.searchParseVersion).toArray();
    if (toUpdate.length === 0 && (await searchHistory.where('searchDate').below(new Date()).first())?.parseVersion === undefined) {
      await searchHistory.filter(s => !s.parseVersion).each(s => toUpdate.push(s));
    }
    for (const record of toUpdate) {
      const parsedQuery = await this.getParsedQuery(record.searchCriteria);
      record.parseVersion = this.searchParseVersion;
      record.parsedQuery = parsedQuery;
      await searchHistory.put(record);
    }
  }

  private updateAutocompleteSuggestions(suggestions: Partial<Suggestion>[]) {
    this.suggestions.update(s => {
      let totalSuggestions = (Object.keys(s) as SuggestionType[]).flatMap(k => s[k]).length;
      const autocomplete: Suggestion[] = suggestions
        .filter(s => s.suggestion && s.suggestion.trim() !== '')
        .map(s => ({ suggestion: s.suggestion, type: 'autocomplete', index: totalSuggestions++ } as Suggestion));
      return { ...s, autocomplete };
    });
  }
}

export class RelativeClipSearchInputFacilitator extends ClipSearchInputFacilitator {
  protected getSearchUrl(searchCriteria: SearchCriteria): string {
    return `${window.location.pathname}?${this.getUrlParams(searchCriteria)}`;
  }
}

export class ShowSearchInputFacilitator implements SearchInputFacilitator {
  private shows: Show[] = [];
  private suggestions = writable({} as Suggestions);
  private input = '';

  constructor(private suggestionCount: number = 10) {
    this.loadShows().then();
  }

  private async loadShows() {
    const showsResponse = await getShows();
    this.shows.push(...showsResponse);
    this.updateInput(this.input);
  }

  updateInput(input: string) {
    const formattedInput = input.trim().toLowerCase();
    const show: Suggestion[] = this.shows.filter(s => s.show.toLowerCase().includes(formattedInput))
      .map(s => ({ suggestion: s.show, type: 'show', index: 0 } as Suggestion))
      .slice(0, this.suggestionCount);
    show.forEach((s, i) => s.index = i);
    this.suggestions.update(s => ({ ...s, show }));
    this.input = input;
  }

  search() {
    const url = `/shows?q=${encodeURIComponent(this.input)}`;
    goto(url);
  }

  getSuggestionStore() {
    return this.suggestions;
  }
}

export class NoopSearchInputFacilitator implements SearchInputFacilitator {
  updateInput() {
  }

  getSuggestionStore() {
    return writable({} as Suggestions);
  }

  search() {
  }
}

export const clipSearchInputFacilitator = browser?new ClipSearchInputFacilitator():new NoopSearchInputFacilitator();
