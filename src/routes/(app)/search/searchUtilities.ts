import type {  SearchCriteria } from './stores';

export function loadStateFromUrl(url:URL):SearchCriteria {

  const params = new URLSearchParams(url.search);
  return {
    q: params.get("q") || undefined,
    page: parseInt(params.get("page")||'0') || undefined,
  };
}
export function buildQueryParams(searchCriteria: SearchCriteria) {
  const urlSearchParams = new URLSearchParams();
  if (searchCriteria.q !== undefined)
    urlSearchParams.set("q", searchCriteria.q);
  if (searchCriteria.show !== undefined)
    urlSearchParams.set("show", searchCriteria.show);
  if (searchCriteria.page !== undefined)
    urlSearchParams.set("page", searchCriteria.page.toString());
  return urlSearchParams;
}