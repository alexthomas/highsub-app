import { getShow } from '$lib/client/content';

export const ssr = false;

export async function load({ params }) {
  const show = await getShow({ id: params.showId });
  return { show };
}