import { persisted } from 'svelte-persisted-store';

export const safeSearchStore = persisted<SafeSearchType>('safeSearch','ON',{storage:'local'});


export type SearchCriteria = {
  q?:string,
  show?:string,
  page?:number,
}

export type SafeSearchType = 'ON'|'OFF'|'ANIME'|'NO_ANIME';
