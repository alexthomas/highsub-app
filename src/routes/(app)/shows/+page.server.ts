import type {  PageServerLoad } from './$types';
import { getCatalogKey } from '$lib/firebase';
import type { Show } from '$lib/types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';

 type SlimShow = Pick<Show, "show"|"summary"|"tags"|
  "indexedEpisodeCount">;

export const load: PageServerLoad = async ({fetch}) => {
  const catalogKey = getCatalogKey('shows');
  const response = await fetch(`${DATA_FUNCTION_URL}/catalog/${encodeURIComponent(catalogKey)}`);
  const catalog = await response.json() as { shows: SlimShow[] };
  if(!response.ok)
    console.dir(catalog);
  const titles = new Set<string>();
  const shows = catalog.shows.filter(s=>{
    if(titles.has(s.show)){
      return false;
    }
    titles.add(s.show);
    return true;
  }).sort((a,b)=>a.show.localeCompare(b.show));
  return { shows };
};