import type { LayoutServerLoad } from './$types';
import { encodeId, getCatalogKey } from '$lib/firebase';
import type { Episode, Show } from '$lib/types';
import { DATA_FUNCTION_URL } from '$lib/ServerUtilities';

export const load: LayoutServerLoad = async ({ params, fetch }) => {
  const showId = encodeId(params.showId);
  const showPromise: Promise<Show> = fetch(`${DATA_FUNCTION_URL}/show/${encodeURIComponent(showId)}`).then(r => r.json());
  const catalogKey = getCatalogKey('episodes', showId);
  const catalogPromise = fetch(`${DATA_FUNCTION_URL}/catalog/${encodeURIComponent(catalogKey)}`).then(r => r.json());
  const [show, catalog] = await Promise.all([showPromise, catalogPromise]);
  const episodes: Episode[] = catalog.episodes.sort((a: Episode, b: Episode) => a.sortKey!.localeCompare(b.sortKey!));
  return { show, episodes };
};
