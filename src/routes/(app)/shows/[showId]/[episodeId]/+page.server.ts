import type { Subtitle } from '$lib/types';
import { DEFAULT_HEADERS, DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import { SERVER_URL } from '$lib/constants';
import type { PageServerLoad } from './$types';
import type { PublicEpisode } from '../../../../../../functions/src/types';

type Response = {episode:PublicEpisode,subtitles:Subtitle[],previousEpisode:PublicEpisode|null,nextEpisode:PublicEpisode|null};
const dataCache = new Map<string,Response>();

export const load: PageServerLoad = async ({ params,fetch }) => {
  const { showId, episodeId } = params;
  if(dataCache.has(episodeId)) return dataCache.get(episodeId)!;
  const {episode,previousEpisode,nextEpisode} = await fetch(`${DATA_FUNCTION_URL}/episode/${encodeURIComponent(showId)}/${encodeURIComponent(episodeId)}/context`).then(r=>r.json());
  const subtitles = await getSubtitles(episode);
  const data = { episode, subtitles, previousEpisode, nextEpisode};
  dataCache.set(episodeId,data);
  return data;
};

async function getSubtitles(episode: PublicEpisode): Promise<Subtitle[]> {
  const url = `${SERVER_URL}/v2/subtitle/episode/${encodeURIComponent(episode.show)}-${episode.season}-${episode.episode}`;
  const response = await fetch(url,{headers:DEFAULT_HEADERS});
  return await response.json();
}