import type { Subtitle } from '$lib/types';
import { applyHeight } from '$lib/utilities';

export function getUrl(subtitle: Subtitle, animated = false): URL {
  const url = applyHeight(subtitle.pngUrl, 150);
  url.pathname = url.pathname.replace(/\.png$/, '.webp');
  if (animated)
    url.searchParams.set('animated', 'true');
  return url;
}
