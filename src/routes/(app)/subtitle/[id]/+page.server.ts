import { parseId } from '$lib/utilities';
import type { Episode, HeadData, Show, Subtitle } from '$lib/types';
import type { PageServerLoad } from './$types';
import { SERVER_URL } from '$lib/constants';
import { DEFAULT_HEADERS, DATA_FUNCTION_URL } from '$lib/ServerUtilities';
import { stitch } from '$lib/client/combinationUtilities';

type fetchType = typeof fetch;
const DEFAULT_CONTEXT_SIZE = 10;
export const load: PageServerLoad = async ({ fetch, params, url }) => {
  let subtitlePromise: Promise<Subtitle | undefined>;
  let showPromise: Promise<Show | undefined>;
  let episodePromise: Promise<Partial<Episode> | undefined>;

  const { show, season, episode } = parseId(params.id)!;
  const episodeId = `${show}-${season}-${episode}`;
  subtitlePromise = getSubtitle(fetch, params.id);
  showPromise = fetch(`${DATA_FUNCTION_URL}/show/${encodeURIComponent(show)}`).then(r => r.json());

  episodePromise = fetch(`${DATA_FUNCTION_URL}/episode/${encodeURIComponent(show)}/${encodeURIComponent(episodeId)}`).then(r => r.json());
  const [subtitle, show1, episode1] = await Promise.all([subtitlePromise, showPromise, episodePromise]);
  if (!subtitle) {
    throw new Error(`Subtitle not found for ${params.id}`);
  }
  const { next, previous } = getContext(url);
  console.log(`${subtitle.id} next: ${next}, previous: ${previous}`);
  const [previousSubtitles, nextSubtitles] = await Promise.all([getPreviousSubtitles(fetch, subtitle, previous), getNextSubtitles(fetch, subtitle, next)]);
  const headData = getHeadData(subtitle, [...previousSubtitles, subtitle, ...nextSubtitles]);
  return {
    show: show1, episode: episode1, next, previous, subtitle,
    previousSubtitles, nextSubtitles,
    headData,
    context: {
      previousSubtitlesPromise: getPreviousSubtitles(fetch, subtitle, Math.max(previous, DEFAULT_CONTEXT_SIZE)),
      nextSubtitlesPromise: getNextSubtitles(fetch, subtitle, Math.max(next, DEFAULT_CONTEXT_SIZE))
    }
  };
};

function getContext(url: URL): { next: number, previous: number } {
  let next = 0, previous = 0;
  if (url.searchParams.has('next'))
    next = parseInt(url.searchParams.get('next') || '0');
  if (url.searchParams.has('previous'))
    previous = parseInt(url.searchParams.get('previous') || '0');
  return { next, previous };
}

const fetchConfig = { headers: { ...DEFAULT_HEADERS, accept: 'application/json' } };

async function getRandomSubtitle(fetch: fetchType): Promise<Subtitle> {
  return await fetch(`${SERVER_URL}/v2/subtitle`, fetchConfig)
    .then(r => r.json());
}

async function getSubtitle(fetch: fetchType, id: string): Promise<Subtitle | undefined> {
  return await fetch(`${SERVER_URL}/v2/subtitle/${encodeURIComponent(id)}`, fetchConfig)
    .then(r => r.json());
}

async function getPreviousSubtitles(fetch: fetchType, subtitle: Subtitle, previous: number): Promise<Subtitle[]> {
  if (previous === 0) return [];
  return await fetch(subtitle.previousSubtitleUrl + '?count=' + Math.max(previous || 0, DEFAULT_CONTEXT_SIZE), fetchConfig).then(r => r.json());
}

async function getNextSubtitles(fetch: fetchType, subtitle: Subtitle, next: number): Promise<Subtitle[]> {
  if (next === 0) return [];
  return await fetch(subtitle.nextSubtitleUrl + '?count=' + Math.max(next || 0, DEFAULT_CONTEXT_SIZE), fetchConfig).then(r => r.json());
}

function getTitle(subtitles: Subtitle[]) {
  let title = '';
  for (let i = 0; i < subtitles.length; i++) {
    title += subtitles[i].text + ' ';
  }

  return title;
}

function getShortTitle(title: string) {
  const words = title.split(' ');
  let shortTitle = '';
  for (let word of words) {
    shortTitle += word + ' ';
    if (shortTitle.length > 20)
      return shortTitle + '...';
  }
  return shortTitle;
}

function getCombinedVideo(subtitle: Subtitle, subtitles: Subtitle[]) {
  return stitch(subtitle.videoUrl, subtitle, subtitles);
}

function applyContentHit(contentUrl: string) {
  const url = new URL(contentUrl);
  url.searchParams.append('countHit', 'true');
  return url.toString();
}

function getHeadData(subtitle: Subtitle, subtitles: Subtitle[]): HeadData {
  const title = getTitle(subtitles);
  const shortTitle = getShortTitle(title);
  return {
    title: shortTitle,
    description: title,
    imageUrl: applyContentHit(subtitle.pngUrl),
    videoUrl: applyContentHit(getCombinedVideo(subtitle, subtitles)),
    url: `https://app.aniclips.alexk8s.com/subtitle/${subtitle.id}`
  };
}