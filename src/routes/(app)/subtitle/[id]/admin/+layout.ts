import { SERVER_URL } from "$lib/constants";

export async function load({ fetch, params, url }) {
  const subtitle = await fetch(`${SERVER_URL}/v1/subtitle/${params.id}`, { headers: { accept: "application/json" } })
    .then(r => r.json());
  return { subtitle};
}