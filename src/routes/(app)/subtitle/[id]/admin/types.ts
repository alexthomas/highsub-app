export type SameActor = { actorName: string, originalShowCharacters: Array<string>, originalShow: string, otherShowCharacters: Array<string>, otherShow: string }
export type EditOutcome = {type:"delete"}|{type:"renameCharacter",newName: string}|{type:'setCharacterRole',newRole:string}
