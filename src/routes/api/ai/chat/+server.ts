import { createOpenAI } from '@ai-sdk/openai';
import { OPENAI_API_KEY } from '$env/static/private';

import type { RequestHandler } from './$types';
import {  streamText } from 'ai';


const openai = createOpenAI({
  apiKey: OPENAI_API_KEY,
  compatibility: 'strict'
});
const model = openai('gpt-4o-mini');

type MessageType = { content: string, role: string,  };
export const POST = (async ({ request,url }) => {
	// Extract the `prompt` from the body of the request
	const { messages: rawMessages } = await request.json();

	const messages = rawMessages.map((message: MessageType) => ({
		content: message.content,
		role: message.role
	}));
  const result = await streamText({ model, messages });
  return result.toDataStreamResponse();
}) satisfies RequestHandler;
