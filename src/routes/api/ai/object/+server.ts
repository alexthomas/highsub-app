import { createOpenAI } from '@ai-sdk/openai';
import { OPENAI_API_KEY } from '$env/static/private';

import type { RequestHandler } from './$types';
import { convertToCoreMessages, generateObject } from 'ai';
import { schemas } from './schemas';
import { ZodObject } from 'zod';
import { error } from '@sveltejs/kit';


const openai = createOpenAI({
  apiKey: OPENAI_API_KEY,
  compatibility: 'strict'
});
const model = openai('gpt-4o-mini');

type MessageType = { content: string, role: string, };
export const POST = (async ({ request }) => {
  // Extract the `prompt` from the body of the request
  const { prompt, schemaName } = await request.json();
  const schema: ZodObject<any> = schemas[schemaName];
  if (!schema)
    return error(500, { message: 'Invalid schema name: ' + schemaName });
  const messages = convertToCoreMessages([{ role: 'user', content: prompt }]);

  const generateObjectResult = await generateObject({ model, messages, schema });
  return generateObjectResult.toJsonResponse();
}) satisfies RequestHandler;
