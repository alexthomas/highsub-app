import { z, type ZodObject } from 'zod';

const adviceSchema = z.object({
  message: z.string().describe('The message to display to the user'),
  suggestions: z.array(z.string()).describe('Suggestions for the user to consider'),
})

export const schemas:{[key:string]:ZodObject<any>} = {advice:adviceSchema}