import type { Readable } from 'svelte/store';
import type { Page } from '@sveltejs/kit';
import type { Preferences } from '$lib/client/appwrite';
import {
  faDice, faGamepad, faHatCowboySide,
  faHome,
  faLayerGroup,
  faLineChart,
  faPage, faRobot,
  faSave,
  faSearch, faSparkle, faTvRetro, faUser, faVectorPolygon, faWarning, type IconDefinition
} from '@fortawesome/pro-solid-svg-icons';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';
import { derived, writable } from 'svelte/store';
import type { Models } from 'appwrite';
import { user } from '$lib/client/auth';
import type { User } from 'firebase/auth';

const navigationState = writable<{ page?: Page, user?: User | undefined|null, isAdmin?: boolean }>({});
export const menuSections = derived(navigationState, ({ page, user, isAdmin }) => getMenuSections(page, user, isAdmin));

export function initializeNavigationState(page: Readable<Page>, account: Readable<Models.User<Preferences> | undefined>, isAdmin: Readable<boolean>) {
  page.subscribe(p => navigationState.update(n => ({ ...n, page: p })));
  user.subscribe(u => navigationState.update(n => ({ ...n, user: u })));
  account.subscribe(a => navigationState.update(n => ({ ...n, account: a })));
  isAdmin.subscribe(i => navigationState.update(n => ({ ...n, isAdmin: i })));
}

type MenuSection = {
  title?: string;
  href?: string;
  items: MenuItem[];
};
type MenuItem = {
  title: string;
  icon?: IconDefinition;
  image?: string;
  href: string;
  target?: string;
};

const mainSection = {
  items: [
    {
      title: 'Home',
      icon: faHome,
      href: '/'
    },
    {
      title: 'Search',
      icon: faSearch,
      href: '/search'
    },
    {
      title: 'Shows',
      icon: faTvRetro,
      href: '/shows'
    },
    {
      title: 'Popular',
      icon: faSparkle,
      href: '/popular'
    },
    {
      title: 'Random',
      icon: faDice,
      href: '/random'
    },
    {
      title: 'Games',
      icon: faGamepad,
      href: '/game'
    },
    {
      title: 'Editor Picks',
      icon: faHatCowboySide,
      href: '/editor-picks'
    }
  ]
};
const clipsSection = {
  title: 'Your Clips',
  items: [
    {
      title: 'Saved',
      icon: faSave,
      href: '/saved'
    },
    {
      title: 'Collections',
      icon: faLayerGroup,
      href: '/collections'
    }
  ]
};
const contactSection = {
  title: 'Contact',
  items: [
    {
      title: 'Report an issue',
      icon: faWarning,
      href: '/feedback/issues'
    },
    {
      title: 'Request a feature',
      icon: faSparkle,
      href: '/feedback/request'
    },
    {
      title: 'Discord',
      icon: faDiscord,
      href: 'https://discord.gg/y2eAM9QvH5',
      target: '_blank'
    },
    {
      title: 'Get Connected',
      icon: faVectorPolygon,
      href: '/connected'
    }
  ]
};
const adminSection = {
  title: 'Admin',
  href: '/admin',
  items: [
    {
      title: 'Shows',
      icon: faLayerGroup,
      href: '/admin/shows'
    },
    {
      title: 'Graphs',
      icon: faLineChart,
      href: '/admin'
    },
    {
      title: 'Generate Article',
      icon: faPage,
      href: '/article/generate'
    },
    {
      title: 'Upcoming Articles',
      icon: faPage,
      href: '/article/upcoming'
    }
  ]
};
const accountSection = {
  title: 'Your Account',
  items: [
    {
      title: 'Profile',
      href: '/profile',
      icon: faUser

    },
    {
      title: 'Settings',
      href: '/settings'
    },
    {
      title: 'Logout',
      href: '/logout'
    }
  ]
};
const loginSection = {
  items: [
    {
      title: 'Login',
      href: '/login'
    },
    {
      title: 'Register',
      href: '/register'
    }
  ]
};

function getMenuSections(page?: Page, user?: User|null, isAdmin?: boolean): MenuSection[] {
  const sections: MenuSection[] = [mainSection];
  if (user) {
    sections.push(clipsSection);
  }
  if (isAdmin) {
    sections.push(adminSection);
  }
  if (user)
    sections.push(accountSection);
  else
    sections.push(loginSection);

  sections.push(contactSection);
  return sections;
}