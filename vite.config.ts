import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig,Plugin } from 'vite';

const corsHeadersPlugin :Plugin= {
	name: 'add cors-headers',
	configureServer({ middlewares }) {
		middlewares.use((req, res, next) => {
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Access-Control-Allow-Headers', '*');
			res.setHeader('Access-Control-Allow-Methods', 'GET');
			next();
		});
	}
}

const config: UserConfig = {
	plugins: [sveltekit(),corsHeadersPlugin],
};

export default config;
